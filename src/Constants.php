<?php

namespace Fstar\ConstGenerater;

class Constants {
    const conf_name = 'fstar-const-generater';
    const lib_parse = 'fs_const_generater_parse';

    const DEL_NO  = 0;
    const DEL_YES = 2;

    const VAL_TYPE_NUMBER      = 'number';
    const VAL_TYPE_STRING      = 'string';
    const VAL_TYPE_CONST       = 'const';
    const VAL_TYPE_MAP         = 'map';
    const VAL_TYPE_BACK_CONST  = 'back_const';
    const VAL_TYPE_FRONT_CONST = 'front_const';
    const VAL_TYPE_BACK_MAP    = 'back_map';
    const VAL_TYPE_FRONT_MAP   = 'front_map';

    const STR_FORMAT_CAMEL       = 'camel';
    const STR_FORMAT_UPPER_SNAKE = 'upper_snake';
    const STR_FORMAT_LOWER_SNAKE = 'lower_snake';
}
