<?php

namespace Fstar\ConstGenerater;

use Fstar\ConstGenerater\Services\ConstAppService;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Fstar\ConstGenerater\Impl\ConstGeneraterParse;

class ConstGeneraterCommand extends Command {
    protected $signature = 'const-generater';

    protected $description = '常量生成';

    public function __construct() {
        parent::__construct();
    }

    public function handle() {
        $app_key = config('fstar-const-generater.app_key');
        (new ConstAppService())->execConstGenerater($app_key);
    }
}