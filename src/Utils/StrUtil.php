<?php

namespace Fstar\ConstGenerater\Utils;

use Fstar\ConstGenerater\Constants;

class StrUtil {

    public static function camelSnakeTransfer($format, $str) {
        if(Constants::STR_FORMAT_CAMEL == $format) {//返回驼峰
            if(strpos($str, '_') !== false) {//给的字符串是下划线分割就转换为驼峰
                $keys     = explode('_', $str);
                $key_arry = [];
                foreach($keys as $idx => $key) {
                    $key_arry[] = $idx > 0 ? ucfirst($key) : $key;
                }
                return implode('', $key_arry);
            }
            return $str;
        } else {//返回下划线分割字符串
            if(strpos($str, '_') === false) {//给的字符串不是下划线分割就转换为下划线
                $key_arry = [];
                $word     = '';
                $str_len  = strlen($str);
                for($idx = 0; $idx < $str_len; $idx++) {
                    $key  = substr($str, $idx, 1);
                    $char = ord($key);
                    if($idx > 0 && $char >= 65 && $char <= 90) {
                        $key_arry[] = $word;
                        $word       = '';
                    }
                    $word .= $key;
                }
                if(strlen($word) > 0) {
                    $key_arry[] = $word;
                }
                $str = implode('_', $key_arry);
            }
            return Constants::STR_FORMAT_LOWER_SNAKE == $format ? strtolower($str) : strtoupper($str);
        }
    }
}