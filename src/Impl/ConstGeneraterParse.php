<?php

namespace Fstar\ConstGenerater\Impl;

use Fstar\ConstGenerater\Api\ConstContentApi;

class ConstGeneraterParse {

    public function getFrontConstGeneraterParse(): ConstContentApi {
        $impl = config('fstar-const-generater.front_impl');
        return $this->createDriver(config("fstar-const-generater.impl.front.{$impl}"), ConstContentApi::class);
    }

    public function getBackConstGeneraterParse(): ConstContentApi {
        $impl = config('fstar-const-generater.back_impl');
        return $this->createDriver(config("fstar-const-generater.impl.back.{$impl}"), ConstContentApi::class);
    }

    private function createDriver($driver_class, $clazz) {
        $oReflectionClass = new \ReflectionClass($driver_class);
        $svc              = $oReflectionClass->newInstance();
        if(!($svc instanceof $clazz)) {
            throw new \Exception("Task {$driver_class} isn't instance of {$clazz}");
        }
        return $svc;
    }
}