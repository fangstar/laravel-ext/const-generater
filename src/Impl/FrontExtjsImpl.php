<?php

namespace Fstar\ConstGenerater\Impl;

use Fstar\ConstGenerater\Api\ConstContentApi;
use Fstar\ConstGenerater\Constants;
use Fstar\ConstGenerater\Utils\StrUtil;
use Illuminate\Support\Facades\File;

class FrontExtjsImpl implements ConstContentApi {
    private $space = '    ';

    public function createConstFile(array $content_map) {
        $space        = $this->space;
        $str_format   = config('fstar-const-generater.front_str_format');
        $group_format = config('fstar-const-generater.front_group_str_format');
        foreach($content_map as $path => $data) {
            $bg_header = $this->initBgHeader($data);
            $content   = $bg_header['content'];
            foreach($data['items'] as $group) {
                $group_key             = StrUtil::camelSnakeTransfer($group_format, $group['group_key']);
                $group['constant_len'] = max($group['constant_len'], 4);
                $content[]             = "\n{$space}/************** {$group['text']} START **************/";
                $content[]             = "{$space}{$space}{$group_key}: {";
                $key                   = str_pad("map", $group['constant_len']);
                $map                   = ["{$space}{$space}{$space}{$key} : {"];
                foreach($group['items'] as $item) {
                    $text         = empty($item['text_color']) ? $item['text'] : "<span style=\"color: {$item['text_color']}\">{$item['text']}</span>";
                    $constant_key = StrUtil::camelSnakeTransfer($str_format, $item['constant_key']);
                    $key          = str_pad($constant_key, $group['constant_len']);
                    
                    if(in_array($item['type'], [Constants::VAL_TYPE_NUMBER, Constants::VAL_TYPE_STRING])) {
                        $val_str   = $item['type'] == Constants::VAL_TYPE_STRING ? "'{$item['val']}'" : $item['val'];
                        $content[] = "{$space}{$space}{$space}{$key}: {$val_str}, //【{$item['text']}】";
                    } elseif(in_array($item['type'], [Constants::VAL_TYPE_CONST, Constants::VAL_TYPE_FRONT_CONST])) {
                        $content[] = "{$space}{$space}{$space}{$key}: {$item['val']}, //【{$item['text']}】";
                    }

                    if(in_array($item['type'], [Constants::VAL_TYPE_NUMBER, Constants::VAL_TYPE_STRING])) {
                        $map[] = "{$space}{$space}{$space}{$space}'{$item['val']}':'{$text}',";
                    } elseif(in_array($item['type'], [Constants::VAL_TYPE_MAP, Constants::VAL_TYPE_FRONT_MAP])) {
                        $map[] = "{$space}{$space}{$space}{$space}'{$item['constant_key']}':{$item['val']},";
                    }
                }
                $map[]     = "{$space}{$space}{$space}}";
                $content[] = implode("\n", $map);
                $content[] = "{$space}{$space}},";
                $content[] = "{$space}/*************** {$group['text']} END ***************/\n";
            }
            $content[] = "{$space}}";
            $content[] = "})";
            $this->writeFile($bg_header['path'], $content);
        }
    }

    private function initBgHeader($data) {
        $path_sep    = DIRECTORY_SEPARATOR;
        $date        = date('Y-m-d H:i:s');
        $app_paths   = explode('/', $data['app_path']);
        $group_paths = explode('/', $data['group_path']);
        $paths       = array_merge($app_paths, $group_paths);
        $dir_arry    = array_slice($paths, 0, count($paths) - 1);
        array_unshift($group_paths, $data['app_ns']);
        $ns        = implode('.', $group_paths);
        $content   = ["/**\n * 文件由 php artisan const-generater 命令生成，请勿手动修改\n */\n"];
        $content[] = "Ext.define('{$ns}', {";
        $content[] = "{$this->space}statics: {";

        foreach($dir_arry as $idx => $dir) {
            $dir_path = base_path(implode($path_sep, array_slice($dir_arry, 0, $idx + 1)));
            if(!is_dir($dir_path)) {
                File::makeDirectory($dir_path);
            }
        }

        $path = implode('/', $paths);
        return ['content' => $content, 'path' => base_path("{$path}.js")];
    }

    private function writeFile($file_path, $content) {
        $handler = fopen($file_path, 'w+');
        fwrite($handler, implode("\r\n", $content));
        fclose($handler);
    }
}
