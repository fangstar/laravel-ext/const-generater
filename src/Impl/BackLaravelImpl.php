<?php

namespace Fstar\ConstGenerater\Impl;

use Fstar\ConstGenerater\Api\ConstContentApi;
use Fstar\ConstGenerater\Utils\StrUtil;
use Illuminate\Support\Facades\File;
use Fstar\ConstGenerater\Constants;

class BackLaravelImpl implements ConstContentApi {

    public function createConstFile(array $content_map) {
        $space      = '    ';
        $str_format = config('fstar-const-generater.back_str_format');
        foreach($content_map as $path => $data) {
            $bg_header = $this->initBgHeader($data);
            $content   = $bg_header['content'];
            foreach($data['items'] as $group) {
                $group_key = StrUtil::camelSnakeTransfer($str_format, $group['group_key']);
                $content[] = "\n{$space}/************** {$group['text']} START **************/";
                $map       = ["{$space}const {$group_key} = ["];
                foreach($group['items'] as $item) {
                    $item_key = StrUtil::camelSnakeTransfer($str_format, $item['key']);
                    $key      = str_pad($item_key, $group['key_len']);

                    if(in_array($item['type'], [Constants::VAL_TYPE_NUMBER, Constants::VAL_TYPE_STRING])) {
                        $val_str   = $item['type'] == Constants::VAL_TYPE_STRING ? "'{$item['val']}'" : $item['val'];
                        $content[] = "{$space}const {$key} = {$val_str}; //【{$item['text']}】";
                    } elseif(in_array($item['type'], [Constants::VAL_TYPE_CONST, Constants::VAL_TYPE_BACK_CONST])) {
                        $content[] = "{$space}const {$key} = {$item['val']}; //【{$item['text']}】";
                    }

                    if(in_array($item['type'], [Constants::VAL_TYPE_NUMBER, Constants::VAL_TYPE_STRING])) {
                        $map[] = "{$space}{$space}self::{$key} => '{$item['text']}',";
                    } elseif(in_array($item['type'], [Constants::VAL_TYPE_MAP, Constants::VAL_TYPE_BACK_MAP])) {
                        $map[] = "{$space}{$space}self::{$key} => {$item['val']},";
                    }
                }
                $map[]     = "{$space}];";
                $content[] = implode("\n", $map);
                $content[] = "{$space}/*************** {$group['text']} END ***************/\n";
            }
            $content[] = '}';
            $this->writeFile($bg_header['path'], $content);
        }
    }

    private function initBgHeader($data) {
        $path_sep    = DIRECTORY_SEPARATOR;
        $date        = date('Y-m-d H:i:s');
        $app_paths   = explode('/', $data['app_path']);
        $group_paths = explode('/', $data['group_path']);
        $paths       = array_merge($app_paths, $group_paths);
        $dir_arry    = array_slice($paths, 0, count($paths) - 1);
        array_unshift($group_paths, $data['app_ns']);
        $ns      = implode('\\', array_slice($group_paths, 0, count($group_paths) - 1));
        $class   = $paths[count($paths) - 1];
        $content = ["<?php\n\n/**\n * 文件由 php artisan const-generater 命令生成，请勿手动修改\n */\n\nnamespace {$ns};\nclass {$class} {"];

        foreach($dir_arry as $idx => $dir) {
            $dir_path = base_path(implode($path_sep, array_slice($dir_arry, 0, $idx + 1)));
            if(!is_dir($dir_path)) {
                File::makeDirectory($dir_path);
            }
        }
        $path = implode('/', $paths);
        return ['content' => $content, 'path' => base_path("{$path}.php")];
    }

    private function writeFile($file_path, $content) {
        $handler = fopen($file_path, 'w+');
        fwrite($handler, implode("\r\n", $content));
        fclose($handler);
    }
}
