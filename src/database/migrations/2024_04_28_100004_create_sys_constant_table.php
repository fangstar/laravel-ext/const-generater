<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSysConstantTable extends Migration {
    public function up() {
        Schema::create('sys_constant', function(Blueprint $table) {
            $table->id('sys_constant_id')->comment('系统常量ID');
            $table->integer('sys_constant_group_id')->nullable(false)->comment('系统常量分组ID');
            $table->string('constant_key', 31)->nullable(false)->comment('常量KEY');
            $table->string('constant_name', 31)->nullable(false)->comment('常量名称');
            $table->string('constant_val', 127)->nullable(false)->comment('常量值');
            $table->string('constant_val_type', 31)->nullable(false)->defaults('int')->comment('常量值类型');
            $table->integer('constant_sort')->nullable(false)->defaults(0)->comment('常量排序 由小到大');
            $table->string('constant_color', 31)->nullable(true)->comment('文本颜色');
            $table->tinyInteger('delete_flag')->default('0')->comment('删除状态');
            $table->bigInteger('created_at')->nullable(false)->comment('创建时间');
            $table->bigInteger('updated_at')->nullable(true)->comment('更新时间');

            $table->index(['constant_sort'], 'constant_key');
            $table->unique(['sys_constant_group_id', 'constant_key'], 'idx_constant_key_unique');
        });
    }

    public function down() {
        Schema::dropIfExists('sys_constant_app');
    }
}
