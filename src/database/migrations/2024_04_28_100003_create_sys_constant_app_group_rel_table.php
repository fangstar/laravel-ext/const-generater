<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSysConstantAppGroupRelTable extends Migration {
    public function up() {
        Schema::create('sys_constant_app_group_rel', function(Blueprint $table) {
            $table->id('sys_constant_app_group_rel_id')->comment('应用和分组关联ID');
            $table->integer('sys_constant_group_id')->comment('系统常量分组ID');
            $table->integer('sys_constant_app_group_id')->comment('系统常量应用分组ID');
            $table->integer('del_rel_id')->default('0')->comment('删除辅助字段');
            $table->tinyInteger('delete_flag')->default('0')->comment('删除状态');
            $table->bigInteger('created_at')->nullable(false)->comment('创建时间');
            $table->bigInteger('updated_at')->nullable(true)->comment('更新时间');

            $table->unique(['sys_constant_group_id', 'sys_constant_app_group_id', 'del_rel_id'], 'idx_sys_constant_group_app_id_unique');
        });
    }

    public function down() {
        Schema::dropIfExists('sys_constant_app_group_rel');
    }
}
