<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSysConstantGroupTable extends Migration {
    public function up() {
        Schema::create('sys_constant_group', function(Blueprint $table) {
            $table->id('sys_constant_group_id')->comment('系统常量分组ID');
            $table->integer('sys_constant_app_group_id')->nullable(false)->comment('应用和分组关联ID');
            $table->string('group_key', 31)->nullable(false)->comment('分组KEY');
            $table->string('group_name', 31)->nullable(false)->comment('分组名称');
            $table->tinyInteger('delete_flag')->default('0')->comment('删除状态');
            $table->bigInteger('created_at')->nullable(false)->comment('创建时间');
            $table->bigInteger('updated_at')->nullable(true)->comment('更新时间');

            $table->index(['group_key'], 'idx_group_key');
            $table->index(['sys_constant_app_group_id'], 'idx_sys_constant_app_group_id');
        });
    }

    public function down() {
        Schema::dropIfExists('sys_constant_group');
    }
}
