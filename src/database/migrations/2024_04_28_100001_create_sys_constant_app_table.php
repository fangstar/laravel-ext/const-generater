<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSysConstantAppTable extends Migration {
    public function up() {
        Schema::create('sys_constant_app', function(Blueprint $table) {
            $table->id('sys_constant_app_id')->comment('系统常量应用ID');
            $table->string('app_key', 31)->nullable(false)->comment('应用KEY');
            $table->string('app_name', 31)->nullable(false)->comment('应用名称');
            $table->string('app_back_path', 127)->nullable(false)->comment('分组后台路径');
            $table->string('app_back_ns', 127)->nullable(false)->comment('名称空间');
            $table->string('app_front_path', 127)->nullable(false)->comment('分组前台路径');
            $table->string('app_front_ns', 127)->nullable(false)->comment('名称空间');
            $table->tinyInteger('delete_flag')->default('0')->comment('删除状态');
            $table->bigInteger('created_at')->nullable(false)->comment('创建时间');
            $table->bigInteger('updated_at')->nullable(true)->comment('更新时间');

            $table->unique(['app_key'], 'idx_app_key_unique');
        });
    }

    public function down() {
        Schema::dropIfExists('sys_constant_app');
    }
}
