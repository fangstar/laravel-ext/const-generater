<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSysConstantAppGroupTable extends Migration {
    public function up() {
        Schema::create('sys_constant_app_group', function(Blueprint $table) {
            $table->id('sys_constant_app_group_id')->comment('应用和分组关联ID');
            $table->integer('sys_constant_app_id')->comment('系统常量应用ID');
            $table->string('app_group_name', 127)->nullable(false)->comment('APP分组名称');
            $table->string('group_back_path', 127)->nullable(false)->comment('分组后台路径');
            $table->string('group_front_path', 127)->nullable(false)->comment('分组前台路径');
            $table->integer('del_rel_id')->default('0')->comment('删除辅助字段');
            $table->tinyInteger('delete_flag')->default('0')->comment('删除状态');
            $table->bigInteger('created_at')->nullable(false)->comment('创建时间');
            $table->bigInteger('updated_at')->nullable(true)->comment('更新时间');

            $table->unique(['sys_constant_app_id','group_back_path', 'del_rel_id'], 'idx_sys_constant_group_back_path_unique');
            $table->unique(['sys_constant_app_id','group_front_path', 'del_rel_id'], 'idx_sys_constant_group_front_path_unique');
        });
    }

    public function down() {
        Schema::dropIfExists('sys_constant_app_group');
    }
}
