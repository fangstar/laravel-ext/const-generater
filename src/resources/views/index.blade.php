<!DOCTYPE html>
<html lang="zh">
    <head>
        <meta charset="UTF-8">
        <title>常量代码生成</title>
        <meta name="renderer" content="webkit">
        <link rel="shortcut icon" href="favicon.ico" type="image/x-icon"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="https://fsres.fangstar.com/static/js/extjs6.2.1/ext-all.js.gz" type="text/javascript"></script>
        <script src="https://fsres.fangstar.com/static/js/extjs6.2.1/theme-triton/theme-triton.js"></script>
        <script src="https://res.fangstar.com/static/js/extjs6.2.1/locale/locale-zh_CN.js"></script>
        <link rel="stylesheet" href="https://fsres.fangstar.com/static/js/extjs6.2.1/theme-triton/resources/theme-triton-all.css">
        <title>const generater</title>
        <style>
        .grid-opt-btn {
            font-size: 14px;
            padding: 0 3px;
            width: auto;
            color: green;
            text-decoration: underline;
        }

        .item-disabled {
            color: #999999;
            text-decoration: none;
            cursor: default;
        }
        </style>

        <script type="text/javascript">
            Ext.application({
                                name    : 'app',
                                paths   : {
                                    app: 'js/const-generater/ui'
                                },
                                requires: [
                                    'app.comp.sys.AppLoading',
                                ],
                                launch  : function () {
                                    app.comp.sys.AppLoading.show();
                                    Ext.tip.QuickTipManager.init();
                                    Ext.create('Ext.container.Viewport', {
                                        layout   : 'fit',
                                        listeners: {
                                            afterrender: function (viewport) {
                                                viewport.add(Ext.create('app.Main'));
                                            }
                                        }
                                    })
                                }
                            })
        </script>
    </head>
    <body>
    
    </body>
</html>