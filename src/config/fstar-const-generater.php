<?php

return [
    'app_key'                => 'erp',     //要生成的应用key
    'db_conn'                => 'base_ro', //常量配置表数据库链接
    'back_impl'              => 'laravel', //后端代码生成实现类
    'front_impl'             => 'extjs',   //前端代码生成实现类
    'front_str_format'       => 'upper_snake',   //前端分组字符串格式 驼峰 camel  大写下划线 upper_snake 小写下划线 lower_snake
    'front_group_str_format' => 'camel',   //前端分组字符串格式 驼峰 camel  大写下划线 upper_snake 小写下划线 lower_snake
    'back_str_format'        => 'upper_snake',   //后端字符串格式 驼峰 camel  大写下划线 upper_snake 小写下划线 lower_snake
    'back_group_str_format'  => 'camel',   //后端字符串格式 驼峰 camel  大写下划线 upper_snake 小写下划线 lower_snake
    'impl'                   => [
        'front' => [
            'extjs' => \Fstar\ConstGenerater\Impl\FrontExtjsImpl::class,
        ],
        'back'  => [
            'laravel' => \Fstar\ConstGenerater\Impl\BackLaravelImpl::class,
        ]
    ]
];