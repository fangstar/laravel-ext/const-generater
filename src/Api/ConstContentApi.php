<?php

namespace Fstar\ConstGenerater\Api;

interface ConstContentApi {
    /**
     * @param array $content_map
     *
     */
    public function createConstFile(array $content_map);
}