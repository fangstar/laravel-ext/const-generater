Ext.define('app.comp.form.ExtraParamsCombobox', {
    extend        : 'Ext.form.field.ComboBox',
    alias         : ['widget.extraparamcombobox'],
    setExtraParams: function (params) {
        var me = this;
        me.getStore().setExtraParams(params);
        me.getStore().reload();
    }
})
