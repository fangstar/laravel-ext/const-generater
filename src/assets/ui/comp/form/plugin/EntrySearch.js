Ext.define('app.comp.form.plugin.EntrySearch', {
    extend  : 'Ext.AbstractPlugin',
    alias   : ['plugin.entrysearch'],
    requires: [],
    init    : function (component) {
        var plugin = this;
        component.on('afterrender', function (form) {
            form.getEl().on('keydown', function (e, t, eOpts) {
                if (e.getKey() == Ext.event.Event.ENTER) {
                    var conf = plugin.pluginConfig;
                    conf.searchMethod && conf.searchMethod.call(conf.scope || component)
                }
            })
        })

    }
});