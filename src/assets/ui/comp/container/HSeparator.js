Ext.define('app.comp.container.HSeparator', {
    extend: 'Ext.container.Container',
    alias : 'widget.hseparator',
    width : 10,
    html  : '<div style="width:5px;height:70%;margin-top:20%; border-right-color:#e1e1e1;border-width:0 1px 0 0;border-style:solid"></div>'
});