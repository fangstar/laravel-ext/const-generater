/**
 * 时间列.
 * @param {type} param1
 * @param {type} param2
 */
Ext.define('app.comp.grid.column.DateTimeColumn', {
    extend         : 'Ext.grid.column.Column',
    alias          : ['widget.datetimecolumn'],
    width          : 150,
    minWidth       : 100,
    align          : 'center',
    format         : 'Y-m-d H:i:s',
    emptyText      : '',
    defaultRenderer: function (v, meta, rec) {
        var self = this;
        if (Ext.isEmpty(v) || v <= 0) {
            return '';
        }
        var date = new Date();
        date.setTime(v * 1000);
        var val = Ext.Date.format(date, self.format);
        if (val != null || val != undefined) {
            meta && (meta.tdAttr = 'data-qtip="' + Ext.String.htmlEncode(val) + '"');
            return val;
        }
        return self.emptyText;
    }
})

