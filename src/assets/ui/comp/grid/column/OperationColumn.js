/**
 * Copyright (c) 2015-2016, fangstar.com
 *
 * All rights reserved.
 */

/**
 * Grid列表操作列Widget
 */
Ext.define('app.comp.grid.column.OperationColumn', {
    extend       : 'Ext.grid.column.Action',
    alias        : ['widget.operationcolumn', 'widget.OperationColumn'],
    menuText     : '',
    align        : 'left',
    text         : '操作',
    tdCls        : 'grid-item-no-focused',//focus时不显示蚂蚁线
    stopSelection: false,
    disabledType : 'disable',//disable:显示灰色，hide：隐藏
// Renderer closure iterates through items creating an <img> element for each and tagging with an identifying
    // class name x-action-col-{n}
    defaultRenderer: function (v, cellValues, record, rowIdx, colIdx, store, view) {
        var me        = this,
            origScope = me.origScope || me,
            items     = Ext.isArray(me.items) ? me.items : [],
            len       = items.length,
            ret       = '';

        ret = Ext.isFunction(me.origRenderer) ? me.origRenderer.apply(scope, arguments) || '' : '';

        cellValues.tdCls += ' ' + Ext.baseCSSPrefix + 'action-col-cell';

        for (var i = 0; i < len; i++) {
            var item = items[i];

            var scope = item.scope || origScope;
            //执行isDisabled方法.
            var disabled = item.disabled || (item.isDisabled ? item.isDisabled.call(scope, view, rowIdx, colIdx, item, record) : false);
            var tooltip = disabled ? null : (item.tooltip || (item.getTip ? item.getTip.apply(scope, arguments) : null));
            var classes = [];
            classes.push(me.actionIconCls);
            classes.push(Ext.baseCSSPrefix + 'action-col-' + String(i));
            classes.push('grid-opt-btn')
            //如果禁用模式是隐藏，不直接不添加html.
            if (disabled) {
                if (me.disabledType == 'hide') {
                    continue;
                }
                classes.push('item-disabled');
            }
            var text = item.text;
            if (Ext.isFunction(item.getText)) {
                text = item.getText(scope, view, rowIdx, colIdx, item, record);
            }
            if (Ext.isFunction(item.getCls)) {
                var cls = item.getCls(scope, view, rowIdx, colIdx, item, record);
                if (!Ext.isEmpty(cls)) {
                    classes.push(cls);
                }
            }
            ret += '<a class="' + classes.join(' ') + '"'
                   + (tooltip ? Ext.String.format(' data-qtip="{0}"', tooltip) : '')
                   + (item.href ? Ext.String.format(' target="_blank" href="{0}"', item.href) : '')
                   + '>' + text + '</a>';
        }
        return ret;
    }
});
