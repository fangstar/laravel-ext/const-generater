Ext.define('app.comp.grid.plugin.CellTips', {
    extend: 'Ext.plugin.Abstract',
    alias : 'plugin.gridcelltips',
    grid  : null,
    init  : function (comp) {
        var me = this;
        var columns = comp.getColumnManager().getColumns()
        for (var idx in columns) {
            var column = columns[idx];
            if (!Ext.isFunction(column.renderer)) {
                column.renderer = function (v, cellValues, record, rowIdx, colIdx, store, view) {
                    var tip = v = Ext.isEmpty(v) ? this.emptyText : Ext.String.htmlEncode(v);
                    return '<span data-qtip="' + tip + '">' + (v || '') + '</span>';
                }
            }
        }
    }
})
