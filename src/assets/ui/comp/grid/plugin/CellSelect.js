Ext.define('app.comp.grid.plugin.CellSelect', {
    extend    : 'Ext.plugin.Abstract',
    alias     : 'plugin.gridcellselect',
    grid      : null,
    init      : function (comp) {
        var me = this;
        me.grid = comp;
        me.grid.getView().setConfig('enableTextSelection', true)
        me.grid.on('cellclick', me._cellClick, me)
    },
    destroy   : function () {
        var me = this;
        me.grid.un('cellclick', me._cellClick, me)
        me.callParent();
    },
    _cellClick: function (view, td, cellIndex, record, tr, rowIndex, e, eOpts) {
        var column = view.getColumnManager().getColumns()[cellIndex]
        if (column.textSelect) {
            var selection = document.getSelection();
            var range = document.createRange();
            range.selectNode(e.target);
            selection.removeAllRanges()
            selection.addRange(range)
        }
    }
})
