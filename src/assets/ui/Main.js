Ext.define('app.Main', {
    extend       : 'Ext.container.Container',
    layout       : 'border',
    requires     : [
        'app.pages.app.List',
        'app.pages.group.List',
        'app.pages.file.List'
    ],
    initComponent: function () {
        var me = this;
        me.items = [
            {
                region : 'north',
                xtype  : 'container',
                html   : 'Const Generater',
                padding: 15,
                style  : {fontSize: '20px', fontWeight: 'bold'}
            },
            {
                xtype : 'tabpanel',
                region: 'center',
                items : [
                    {xtype: 'applist', title: '应用管理'},
                    {xtype: 'grouplist', title: '常量管理'},
                    {xtype: 'filelist', title: '生成代码'},
                ]
            }
        ];
        me.callParent(arguments);
    },
    afterRender  : function () {
        var me = this;
        me.callParent(arguments);
        app.comp.sys.AppLoading.hide();
    }
});
