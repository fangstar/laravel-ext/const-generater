Ext.define('app.pages.constant.AddEditWin', {
    extend         : 'Ext.window.Window',
    requires       : [
        'app.svc.ConstantSvc',
        'app.comp.container.HSeparator',
        'app.comp.form.plugin.ClearTrigger'
    ],
    layout         : 'fit',
    modal          : true,
    constantStore  : null,
    constantRec    : null,
    groupRec       : null,
    width          : 400,
    height         : 365,
    resizable      : false,
    constrainHeader: true,
    bind           : {title: '{title}'},
    viewModel      : {data: {title: '添加常量'}},
    initComponent  : function () {
        var me = this;
        var typeStore = Ext.create('Ext.data.Store', {
            data: [
                {name: 'number', text: 'number'},
                {name: 'string', text: 'string'},
                {name: 'const', text: 'const'},
                {name: 'map', text: 'map'},
                {name: 'back_const', text: 'back_const'},
                {name: 'front_const', text: 'front_const'},
                {name: 'back_map', text: 'back_map'},
                {name: 'front_map', text: 'front_map'},
            ]
        })
        me.items = [
            {
                xtype: 'form', margin: 10, layout: {type: 'vbox', align: 'stretch'}, defaults: {xtype: 'fieldcontainer', layout: 'hbox', defaults: {flex: 1, labelWidth: 70, labelAlign: 'right', allowBlank: false, allowOnlyWhitespace: false, plugins: ['cleartrigger']}}, items: [
                    {xtype: 'textfield', fieldLabel: 'const id', name: 'sys_constant_id', hidden: true},
                    {items: [{xtype: 'textfield', fieldLabel: '常量KEY', name: 'constant_key'}]},
                    {items: [{xtype: 'textfield', fieldLabel: '常量名称', name: 'constant_name'}]},
                    {items: [{xtype: 'textfield', fieldLabel: '常量值', name: 'constant_val'}]},
                    {items: [{xtype: 'combobox', fieldLabel: '常量类型', name: 'constant_val_type', store: typeStore, editable: false}]},
                    {items: [{xtype: 'numberfield', fieldLabel: '常量排序', name: 'constant_sort', minValue: 1}]},
                    {items: [{xtype: 'textfield', fieldLabel: '文本颜色', name: 'constant_color', allowBlank: true, allowOnlyWhitespace: true}]}
                ]
            }
        ];
        me.buttons = [
            {text: '保存', handler: me._saveClick, scope: me},
            {text: '取消', handler: me.destroy, scope: me}
        ]
        me.callParent(me);
        me.form = me.down('form');
        if (me.constantRec != null) {
            me.getViewModel().set({title: '编辑常量'});
            me.form.getForm().setValues(me.constantRec.getData())
        }
    },
    _saveClick     : function () {
        var me = this;
        if (!me.form.isValid()) {
            return;
        }
        var params = me.form.getValues();
        params['sys_constant_group_id'] = me.groupRec.get('sys_constant_group_id');
        var svc = me.constantRec == null ? app.svc.ConstantSvc.constantAdd : app.svc.ConstantSvc.constantUpd;
        svc.call(me, params).then(function (data) {
            if (me.constantRec) {
                me.constantRec.set(data)
                me.constantRec.commit();
            } else {
                me.constantStore.loadRawData([data], true)
            }
            me.destroy()
        })
    }
});
