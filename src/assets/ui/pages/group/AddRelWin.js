Ext.define('app.pages.group.AddRelWin', {
    extend         : 'Ext.window.Window',
    requires       : [
        'app.svc.GroupSvc',
        'app.comp.form.ExtraParamsCombobox',
        'app.comp.form.plugin.ClearTrigger'
    ],
    layout         : 'fit',
    modal          : true,
    groupStore     : null,
    groupSelection : null,
    width          : 500,
    height         : 200,
    resizable      : false,
    constrainHeader: true,
    bind           : {title: '{title}'},
    viewModel      : {data: {title: '添加分组引用'}},
    initComponent  : function () {
        var me = this;
        me.appNameStore = app.svc.AppSvc.appNameStore(true);
        me.appGroupNameStore = app.svc.AppGroupSvc.nameStore(true);
        me.items = [
            {
                xtype: 'form', margin: 10, layout: {type: 'vbox', align: 'stretch'}, defaults: {labelWidth: 70, labelAlign: 'right', allowBlank: false, allowOnlyWhitespace: false, plugins: ['cleartrigger']}, items: [
                    {xtype: 'combobox', fieldLabel: '应用名称', name: 'sys_constant_app_id', bind: {value: '{sys_constant_app_id}', disabled: '{sys_constant_group_id}'}, valueField: 'value', store: me.appNameStore, editable: false},
                    {xtype: 'extraparamcombobox', fieldLabel: '分组名称', name: 'sys_constant_app_group_id', bind: {extraParams: {sys_constant_app_id: '{sys_constant_app_id}'}, disabled: '{sys_constant_group_id}'}, valueField: 'value', store: me.appGroupNameStore, editable: false},
                ]
            }
        ];
        me.buttons = [
            {text: '保存', handler: me._saveClick, scope: me},
            {text: '取消', handler: me.destroy, scope: me}
        ]
        me.callParent(me);
        me.form = me.down('form');
    },
    _saveClick     : function () {
        var me = this;
        if (!me.form.isValid()) {
            return;
        }
        var constGroupIds = [];
        for (var idx in me.groupSelection) {
            constGroupIds.push(me.groupSelection[idx].get('sys_constant_group_id'))
        }
        var values = me.form.getValues();
        app.svc.GroupSvc.groupAddRel.call(me, constGroupIds, values.sys_constant_app_group_id).then(function () {
            me.groupStore.reload();
            me.destroy()
        })
    }
});
