Ext.define('app.pages.group.RemoveRelWin', {
    extend           : 'Ext.window.Window',
    requires         : [
        'app.svc.GroupSvc',
    ],
    layout           : 'fit',
    modal            : true,
    groupStore       : null,
    groupSelection   : null,
    width            : 400,
    minHeight        : 100,
    resizable        : false,
    constrainHeader  : true,
    bind             : {title: '{title}'},
    viewModel        : {data: {title: '选择要移除的应用分组'}},
    initComponent    : function () {
        var me = this;
        me.appNameStore = app.svc.AppSvc.appNameStore(true);
        me.appGroupNameStore = app.svc.AppGroupSvc.nameStore(true);
        me.items = [
            {
                xtype: 'form', margin: 10, items: [
                    {xtype: 'checkboxgroup', columns: 2, vertical: true}
                ]
            }
        ];
        me.buttons = [
            {text: '保存', handler: me._saveClick, scope: me},
            {text: '取消', handler: me.destroy, scope: me}
        ]
        me.callParent(me);
        me.form = me.down('form');
        me.checkboxgroup = me.form.down('checkboxgroup');
    },
    listeners        : {
        afterrender: function () {
            var me = this;
            app.svc.GroupSvc.groupQueryRel.call(me, me._getConstGroupIds()).then(function (data) {
                var items = [];
                for (var idx in data) {
                    items.push({boxLabel: data[idx].app_name + ' - ' + data[idx].app_group_name, name: 'sys_constant_app_group_id', inputValue: data[idx].sys_constant_app_group_id});
                }
                me.checkboxgroup.add(items)
            })
        }
    },
    _saveClick       : function () {
        var me = this;
        if (!me.form.isValid()) {
            return;
        }
        var values = me.form.getValues();
        if (!values.sys_constant_app_group_id) {
            return;
        }
        var appGroupIds = Ext.isArray(values.sys_constant_app_group_id) ? values.sys_constant_app_group_id : [values.sys_constant_app_group_id];
        app.svc.GroupSvc.groupRemoveRel.call(me, me._getConstGroupIds(), appGroupIds).then(function () {
            me.groupStore.reload();
            me.destroy()
        })
    },
    _getConstGroupIds: function () {
        var me = this;
        var constGroupIds = [];
        for (var idx in me.groupSelection) {
            constGroupIds.push(me.groupSelection[idx].get('sys_constant_group_id'))
        }
        return constGroupIds;
    }
});
