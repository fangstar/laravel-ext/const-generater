Ext.define('app.pages.group.List', {
    extend             : 'Ext.container.Container',
    requires           : [
        'app.util.Msg',
        'app.svc.AppSvc',
        'app.svc.GroupSvc',
        'app.svc.ConstantSvc',
        'app.svc.AppGroupSvc',
        'app.comp.container.HSeparator',
        'app.comp.form.ExtraParamsCombobox',
        'app.comp.grid.plugin.CellTips',
        'app.comp.grid.plugin.CellSelect',
        'app.comp.grid.column.DateTimeColumn',
        'app.comp.grid.column.OperationColumn'
    ],
    layout             : 'border',
    alias              : 'widget.grouplist',
    viewModel          : {data: {relBtnText: '添加'}},
    initComponent      : function () {
        var me = this;
        me.store = app.svc.GroupSvc.groupStore(true);
        me.constantStore = app.svc.ConstantSvc.constantStore(false);
        me.appNameStore = app.svc.AppSvc.appNameStore();
        me.appGroupStore = app.svc.AppGroupSvc.store();
        me.appGroupNameStore = app.svc.AppGroupSvc.nameStore();
        me.items = [
            {
                xtype      : 'gridpanel', name: 'group', store: me.store, region: 'center', reference: 'group', selModel: {type: 'checkboxmodel'}, split: true, columns: [
                    {dataIndex: 'app_name', text: '应用名称', width: 80},
                    {
                        dataIndex: 'app_group_name', text: '应用组名称', width: 160, renderer: function (value, metaData, record, rowIndex, colIndex, store, view) {
                            var tips = record.get('group_info').replace(/,/, '<br/>');
                            return Ext.String.format('<div data-qtip="{0}">{1}</div>', tips, value);
                        }
                    },
                    {dataIndex: 'group_key', text: '常量组key', flex: 1},
                    {dataIndex: 'group_name', text: '常量组名', flex: 1},
                    {
                        xtype: 'operationcolumn', minWidth: 80, width: 80, items: [
                            {text: '编辑', handler: me._editGroup, scope: me},
                            {text: '删除', handler: me._delGroup, scope: me}
                        ]
                    }
                ],
                tbar       : [
                    {
                        xtype: 'form', layout: 'hbox', name: 'group', items: [
                            {xtype: 'combobox', name: 'sys_constant_app_id', bind: '{sys_constant_app_id}', emptyText: '应用名称', width: 140, valueField: 'value', store: me.appNameStore, editable: false},
                            {xtype: 'extraparamcombobox', name: 'sys_constant_app_group_id', bind: {extraParams: {sys_constant_app_id: '{sys_constant_app_id}'}}, emptyText: '分组名称', width: 140, valueField: 'value', store: me.appGroupNameStore, editable: false},
                            {xtype: 'textfield', name: 'search', emptyText: '常量组KEY或名称查询'},
                            {xtype: 'hseparator'},
                            {xtype: 'button', text: '重置', handler: me._reset, scope: me},
                            {xtype: 'hseparator'},
                            {xtype: 'button', text: '查询', handler: me._search, scope: me},
                        ]
                    },
                    '-',
                    {text: '添加', handler: me._addGroup, scope: me},
                    '-',
                    {text: '关联APP组', handler: me._addGroupRel, bind: {disabled: '{!group.selection}'}, scope: me},
                    '-',
                    {text: '移除APP组', handler: me._removeGroupRel, bind: {disabled: '{!group.selection}'}, scope: me},
                ],
                listeners  : {itemclick: me._griditemclick, scope: me},
                dockedItems: [{xtype: 'pagingtoolbar', dock: 'bottom', displayInfo: true}],
                plugins    : ['gridcelltips', 'gridcellselect']
            },
            {
                xtype  : 'gridpanel', name: 'constant', split: true, region: 'east', width: '60%', store: me.constantStore, columns: [
                    {dataIndex: 'constant_key', text: '常量key', width: 150},
                    {dataIndex: 'constant_name', text: '常量名称', width: 150},
                    {dataIndex: 'constant_val', text: '常量值', width: 100},
                    {dataIndex: 'constant_val_type', text: '常量类型', width: 80},
                    {dataIndex: 'constant_sort', text: '常量排序', width: 80},
                    {dataIndex: 'constant_color', text: '文本颜色', width: 90},
                    {dataIndex: 'created_at', text: '创建时间', xtype: 'datetimecolumn'},
                    {
                        xtype: 'operationcolumn', minWidth: 80, flex: 1, items: [
                            {text: '编辑', handler: me._editConstant, scope: me},
                            {text: '删除', handler: me._delConstant, scope: me}
                        ]
                    }
                ],
                tbar   : [
                    {xtype: 'form', bind: {disabled: '{!group.selection}'}, items: [{xtype: 'button', text: '查询', handler: me._searchConstant, scope: me}]}, '-',
                    {text: '添加', handler: me._addConstant, scope: me, bind: {disabled: '{!group.selection}'}},
                ],
                plugins: ['gridcelltips', 'gridcellselect']
            }
        ];
        me.callParent(me);
        me.form = me.down('form[name=group]')
        me.grid = me.down('gridpanel[name=group]')
        me.relform = me.down('form[name=appgrouprel]')
        me.groupgrid = me.down('gridpanel[name=constant]')
    },
    _resetGroup        : function () {
        var me = this
        me.appgroupform.reset()
        me._search()
    },
    _searchGroup       : function () {
        var me = this
        var values = me.appgroupform.getValues();
        me.appGroupStore.load({params: values})
    },
    _reset             : function () {
        var me = this
        me.form.reset()
        me._search()
    },
    _search            : function () {
        var me = this
        var values = me.form.getValues();
        me.store.load({params: values})
    },
    _griditemclick     : function (view, record, item, index, e, eOpts) {
        var me = this;
        me.constantStore.load({params: {sys_constant_group_id: record.get('sys_constant_group_id')}})
        me.groupRec = record;
    },
    _addGroup          : function () {
        var me = this;
        me._addEditWin();
    },
    _editGroup         : function (grid, rowIdx, colIdx, btn, e, record, dom) {
        var me = this;
        me._addEditWin(record);
    },
    _addEditWin        : function (record) {
        var me = this;
        Ext.create('app.pages.group.AddEditWin', {groupStore: me.store, groupRec: record}).show();
    },
    _delGroup          : function (grid, rowIdx, colIdx, btn, e, record, dom) {
        var me = this;
        app.util.Msg.confirm('提醒', Ext.String.format('确认要删除分组【{0}】吗?', record.get('group_name'))).then(function () {
            app.svc.GroupSvc.groupDel.call(me, record.get('sys_constant_group_id')).then(function () {
                me.store.remove(record);
            })
        })
    },
    _addGroupRel       : function () {
        var me = this;
        var selection = me.grid.getSelection();
        Ext.create('app.pages.group.AddRelWin', {groupStore: me.store, groupSelection: selection}).show();
    },
    _removeGroupRel    : function () {
        var me = this;
        var selection = me.grid.getSelection();
        Ext.create('app.pages.group.RemoveRelWin', {groupStore: me.store, groupSelection: selection}).show();
    },
    _searchConstant    : function () {
        var me = this;
        me.constantStore.reload();
    },
    _addConstant       : function () {
        var me = this;
        me._addEditConstantWin()
    },
    _resetConstant     : function () {
        var me = this;
        me.relform.reset()
    },
    _editConstant      : function (grid, rowIdx, colIdx, btn, e, record, dom) {
        var me = this;
        me._addEditConstantWin(record);
    },
    _addEditConstantWin: function (record) {
        var me = this;
        Ext.create('app.pages.constant.AddEditWin', {constantStore: me.constantStore, constantRec: record, groupRec: me.groupRec}).show();
    },
    _delConstant       : function (grid, rowIdx, colIdx, btn, e, record, dom) {
        var me = this;
        app.util.Msg.confirm('提醒', Ext.String.format('确认要删除常量【{0}】吗?', record.get('constant_name'))).then(function () {
            app.svc.ConstantSvc.constantDel.call(me, record.get('sys_constant_id')).then(function () {
                me.constantStore(record);
            })
        })
    }
});
