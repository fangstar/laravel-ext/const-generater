Ext.define('app.pages.group.AddEditWin', {
    extend         : 'Ext.window.Window',
    requires       : [
        'app.svc.AppSvc',
        'app.svc.GroupSvc',
        'app.svc.AppGroupSvc',
        'app.comp.container.HSeparator',
        'app.comp.form.ExtraParamsCombobox',
        'app.comp.form.plugin.ClearTrigger'
    ],
    layout         : 'fit',
    modal          : true,
    groupStore     : null,
    groupRec       : null,
    width          : 500,
    height         : 280,
    resizable      : false,
    constrainHeader: true,
    bind           : {title: '{title}'},
    viewModel      : {data: {title: '添加分组'}},
    initComponent  : function () {
        var me = this;
        me.appNameStore = app.svc.AppSvc.appNameStore(true);
        me.appGroupNameStore = app.svc.AppGroupSvc.nameStore(true);
        me.items = [
            {
                xtype: 'form', margin: 10, layout: {type: 'vbox', align: 'stretch'}, defaults: {labelWidth: 70, labelAlign: 'right', allowBlank: false, allowOnlyWhitespace: false, plugins: ['cleartrigger']}, items: [
                    {xtype: 'textfield', fieldLabel: 'group id', name: 'sys_constant_group_id', allowBlank: true, allowOnlyWhitespace: true, hidden: true},
                    {xtype: 'combobox', fieldLabel: '应用名称', name: 'sys_constant_app_id', bind: {value: '{sys_constant_app_id}', disabled: '{sys_constant_group_id}'}, valueField: 'value', store: me.appNameStore, editable: false},
                    {xtype: 'extraparamcombobox', fieldLabel: '分组名称', name: 'sys_constant_app_group_id', bind: {extraParams: {sys_constant_app_id: '{sys_constant_app_id}'}, disabled: '{sys_constant_group_id}'}, valueField: 'value', store: me.appGroupNameStore, editable: false},
                    {xtype: 'textfield', fieldLabel: '分组KEY', name: 'group_key'},
                    {xtype: 'textfield', fieldLabel: '分组名称', name: 'group_name'}
                ]
            }
        ];
        me.buttons = [
            {text: '保存', handler: me._saveClick, scope: me},
            {text: '取消', handler: me.destroy, scope: me}
        ]
        me.callParent(me);
        me.form = me.down('form');
        if (me.groupRec != null) {
            me.getViewModel().set({title: '编辑应用', sys_constant_group_id: me.groupRec.get('sys_constant_group_id')});
            me.form.getForm().setValues(me.groupRec.getData())
        }
    },
    _saveClick     : function () {
        var me = this;
        if (!me.form.isValid()) {
            return;
        }
        var svc = me.groupRec == null ? app.svc.GroupSvc.groupAdd : app.svc.GroupSvc.groupUpd;
        svc.call(me, me.form.getValues()).then(function (data) {
            if (me.groupRec) {
                me.groupRec.set(data)
                me.groupRec.commit();
            } else {
                me.groupStore.loadRawData([data], true)
            }
            me.destroy()
        })
    }
});
