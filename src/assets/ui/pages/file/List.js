Ext.define('app.pages.file.List', {
    extend        : 'Ext.panel.Panel',
    requires      : [
        'app.util.Msg',
        'app.svc.FileSvc',
        'app.comp.container.HSeparator',
        'app.comp.form.plugin.EntrySearch',
        'app.comp.grid.plugin.CellTips',
        'app.comp.grid.plugin.CellSelect',
        'app.comp.grid.column.DateTimeColumn',
        'app.comp.grid.column.OperationColumn'
    ],
    alias         : 'widget.filelist',
    viewModel     : {data: {content: ''}},
    layout        : 'border',
    initComponent : function () {
        var me = this;
        me.treeStore = Ext.create('Ext.data.TreeStore')
        me.items = [
            {
                xtype    : 'treepanel', region: 'west', width: 300, rootVisible: false, store: me.treeStore, split: true,
                tbar     : [{text: '刷新', handler: me._treeRefresh, scope: me}],
                listeners: {itemclick: me._treeItemClick, scope: me}
            },
            {xtype: 'panel', region: 'center', scrollable: true, bind: {html: '<div style="display: flex;flex-direction: column">{content}</div>'}}
        ];
        me.callParent(me);
        me.tree = me.down('treepanel')
    },
    afterRender   : function () {
        var me = this;
        me.callParent(me);
        me._treeRefresh();
    },
    _treeRefresh  : function () {
        var me = this;
        app.svc.FileSvc.fileList.call(me.tree).then(function (data) {
            var backData = me._parseData(data.back_info, '后端')
            var frontData = me._parseData(data.front_info, '前端')
            var node = {text: 'erp', id: 'root', children: [backData, frontData]};
            var store = Ext.create('Ext.data.TreeStore', {
                root: {text: 'erp', id: 'root', children: [backData, frontData]}
            });
            me.tree.setStore(store);
        })
    },
    _parseData    : function (data, text) {
        var retData = {}
        for (var path in data) {
            var item = data[path]
            if (item.id == item.pid) {
                retData = item;
                continue;
            }
            if (data[item.pid]) {
                data[item.pid].children.unshift(item)
            }
        }
        retData.text = text;
        return retData;
    },
    _treeItemClick: function (tree, record, item, index, e, eOpts) {
        var me = this;
        if (record.get('leaf')) {
            app.svc.FileSvc.fileDetail.call(me, record.get('path'), record.get('type')).then(function (content) {
                var htmls = [];
                var list = Ext.String.htmlEncode(content).replace(/ /g, '&nbsp;&nbsp;').split("\n")
                for (var idx in list) {
                    htmls.push(Ext.String.format('<div style="display: flex;flex-direction: row;">' +
                                                 '  <div style="width: 60px;background-color: #EEEEEE;border-right: 1px solid #AAAAAA;border-bottom: 1px solid #DEDEDE;padding: 2px 3px;">{0}</div>' +
                                                 '  <div style="flex: 1;padding: 2px 5px">{1}</div>' +
                                                 '</div>', idx + 1, list[idx]))
                }
                me.getViewModel().set({content: htmls.join('')})
            })
        }
    },
});