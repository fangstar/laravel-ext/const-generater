Ext.define('app.pages.app.AddEditWin', {
    extend         : 'Ext.window.Window',
    requires       : [
        'app.svc.AppSvc',
        'app.comp.container.HSeparator',
        'app.comp.form.plugin.ClearTrigger'
    ],
    layout         : 'fit',
    modal          : true,
    appStore       : null,
    appRec         : null,
    width          : 600,
    height         : 320,
    resizable      : false,
    constrainHeader: true,
    bind           : {title: '{title}'},
    viewModel      : {data: {title: '添加应用'}},
    initComponent  : function () {
        var me = this;
        me.items = [
            {
                xtype: 'form', margin: 10, layout: {type: 'vbox', align: 'stretch'}, defaults: {xtype: 'fieldcontainer', layout: 'hbox', defaults: {flex: 1, labelWidth: 90, labelAlign: 'right', allowBlank: false, allowOnlyWhitespace: false, plugins: ['cleartrigger']}}, items: [
                    {
                        items: [
                            {xtype: 'textfield', fieldLabel: 'app id', name: 'sys_constant_app_id', hidden: true, allowBlank: true, allowOnlyWhitespace: true},
                            {xtype: 'textfield', fieldLabel: 'app key', name: 'app_key'},
                            {xtype: 'textfield', fieldLabel: 'app名称', name: 'app_name'},
                        ]
                    },
                    {items: [{xtype: 'textfield', fieldLabel: '后台路径', name: 'app_back_path'}]},
                    {items: [{xtype: 'textfield', fieldLabel: '后台名称空间', name: 'app_back_ns'}]},
                    {items: [{xtype: 'textfield', fieldLabel: '前台路径', name: 'app_front_path'}]},
                    {items: [{xtype: 'textfield', fieldLabel: '前台名称空间', name: 'app_front_ns'}]},
                ]
            }
        ];
        me.buttons = [
            {text: '保存', handler: me._saveClick, scope: me},
            {text: '取消', handler: me.destroy, scope: me}
        ]
        me.callParent(me);
        me.form = me.down('form');
        if (me.appRec != null) {
            me.getViewModel().set({title: '编辑应用'});
            me.form.getForm().setValues(me.appRec.getData())
        }
    },
    _saveClick     : function () {
        var me = this;
        if (!me.form.isValid()) {
            return;
        }
        var svc = me.appRec == null ? app.svc.AppSvc.appAdd : app.svc.AppSvc.appUpd;
        svc.call(me, me.form.getValues()).then(function (data) {
            me.appStore.load()
            me.destroy()
        })
    }
});
