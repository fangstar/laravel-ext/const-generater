Ext.define('app.pages.app.GroupAddEditWin', {
    extend         : 'Ext.window.Window',
    requires       : [
        'app.svc.AppSvc',
        'app.svc.AppGroupSvc',
        'app.comp.container.HSeparator',
        'app.comp.form.plugin.ClearTrigger'
    ],
    layout         : 'fit',
    modal          : true,
    appGroupStore  : null,
    appGroupRec    : null,
    width          : 500,
    height         : 280,
    resizable      : false,
    constrainHeader: true,
    bind           : {title: '{title}'},
    viewModel      : {data: {title: '添加应用'}},
    initComponent  : function () {
        var me = this;
        me.appNameStore = app.svc.AppSvc.appNameStore();
        me.items = [
            {
                xtype: 'form', margin: 10, layout: {type: 'vbox', align: 'stretch'}, defaults: {labelWidth: 80, labelAlign: 'right', allowBlank: false, allowOnlyWhitespace: false, plugins: ['cleartrigger']}, items: [
                    {xtype: 'textfield', fieldLabel: '分组ID', name: 'sys_constant_app_group_id', hidden: true, allowBlank: true, allowOnlyWhitespace: true},
                    {xtype: 'combobox', fieldLabel: '应用名称', name: 'sys_constant_app_id', valueField: 'value', store: me.appNameStore, editable: false},
                    {xtype: 'textfield', fieldLabel: '分组名称', name: 'app_group_name'},
                    {xtype: 'textfield', fieldLabel: '后台路径', name: 'group_back_path'},
                    {xtype: 'textfield', fieldLabel: '前台路径', name: 'group_front_path'}
                ]
            }
        ];
        me.buttons = [
            {text: '保存', handler: me._saveClick, scope: me},
            {text: '取消', handler: me.destroy, scope: me}
        ]
        me.callParent(me);
        me.form = me.down('form');
        if (me.appGroupRec != null) {
            me.getViewModel().set({title: '编辑应用'});
            me.form.getForm().setValues(me.appGroupRec.getData())
        }
    },
    _saveClick     : function () {
        var me = this;
        if (!me.form.isValid()) {
            return;
        }
        var svc = me.appGroupRec == null ? app.svc.AppGroupSvc.add : app.svc.AppGroupSvc.upd;
        svc.call(me, me.form.getValues()).then(function (data) {
            me.appGroupStore.load()
            me.destroy()
        })
    }
});
