Ext.define('app.pages.app.List', {
    extend          : 'Ext.container.Container',
    requires        : [
        'app.util.Msg',
        'app.svc.AppSvc',
        'app.svc.GroupSvc',
        'app.svc.AppGroupSvc',
        'app.comp.container.HSeparator',
        'app.comp.form.plugin.EntrySearch',
        'app.comp.grid.plugin.CellTips',
        'app.comp.grid.plugin.CellSelect',
        'app.comp.grid.column.DateTimeColumn',
        'app.comp.grid.column.OperationColumn'
    ],
    layout          : 'border',
    alias           : 'widget.applist',
    viewModel       : {data: {sys_constant_app_id: null}},
    initComponent   : function () {
        var me = this;
        me.store = app.svc.AppSvc.appStore();
        me.appNameStore = app.svc.AppSvc.appNameStore();
        me.groupStore = app.svc.AppGroupSvc.store(true);
        me.items = [
            {
                xtype    : 'gridpanel', name: 'app', store: me.store, region: 'center', reference: 'app', columns: [
                    {dataIndex: 'app_key', text: '应用key', width: 100},
                    {dataIndex: 'app_name', text: '应用名称', width: 100},
                    {dataIndex: 'app_back_path', text: '后台路径', width: 180},
                    {dataIndex: 'app_front_path', text: '前台路径', width: 160},
                    {dataIndex: 'app_back_ns', text: '后台名称空间', width: 180},
                    {dataIndex: 'app_front_ns', text: '前台名称空间', width: 160},
                    {dataIndex: 'created_at', text: '创建时间', xtype: 'datetimecolumn'},
                    {
                        xtype: 'operationcolumn', minWidth: 60, flex: 1, items: [
                            {text: '编辑', handler: me._editApp, scope: me},
                            {text: '删除', handler: me._delApp, scope: me},
                            {text: '代码生成', handler: me._createCode, scope: me},
                        ]
                    }
                ],
                tbar     : [
                    {
                        xtype: 'form', layout: 'hbox', name: 'app', items: [
                            {xtype: 'textfield', name: 'search', emptyText: 'app key或名称查询'},
                            {xtype: 'hseparator'},
                            {xtype: 'button', text: '重置', handler: me._reset, scope: me},
                            {xtype: 'hseparator'},
                            {xtype: 'button', text: '查询', handler: me._search, scope: me},
                        ]
                    },
                    '-',
                    {text: '添加', handler: me._addApp, scope: me}
                ],
                listeners: {itemclick: me._griditemclick, scope: me},
                plugins  : ['gridcelltips', 'gridcellselect']
            },
            {
                xtype      : 'gridpanel', name: 'group', region: 'east', width: 700, split: true, store: me.groupStore, columns: [
                    {dataIndex: 'app_name', text: 'APP名称', width: 140},
                    {dataIndex: 'app_group_name', text: '分组名称', width: 140},
                    {dataIndex: 'group_back_path', text: '后台路径', flex: 1, editor: {allowBlank: false}},
                    {dataIndex: 'group_front_path', text: '前台路径', flex: 1, editor: {allowBlank: false}},
                    {
                        xtype: 'operationcolumn', minWidth: 80, width: 80, items: [
                            {text: '编辑', handler: me._editAppGroup, scope: me},
                            {text: '删除', handler: me._delAppGroup, scope: me},
                        ]
                    }
                ],
                tbar       : [{
                    xtype: 'form', name: 'group', layout: 'hbox', plugins: [{ptype: 'entrysearch', searchMethod: me._groupSearch, scope: me}], items: [
                        {xtype: 'combobox', emptyText: '应用名称', name: 'sys_constant_app_id', valueField: 'value', store: me.appNameStore, editable: false, bind: '{sys_constant_app_id}'},
                        {xtype: 'textfield', name: 'keyword', emptyText: '分组名称查询'},
                        {xtype: 'hseparator'},
                        {xtype: 'button', text: '重置', handler: me._groupReset, scope: me},
                        {xtype: 'hseparator'},
                        {xtype: 'button', text: '查询', handler: me._groupSearch, scope: me},
                        {xtype: 'hseparator'},
                        {xtype: 'button', text: '添加', handler: me._addAppGroup, scope: me}
                    ]
                }],
                dockedItems: [{xtype: 'pagingtoolbar', dock: 'bottom', displayInfo: true}]
            }
        ];
        me.callParent(me);
        me.form = me.down('form[name=app]')
        me.groupform = me.down('form[name=group]')
        me.grid = me.down('gridpanel[name=app]')
        me.groupgrid = me.down('gridpanel[name=group]')
    },
    _reset          : function () {
        var me = this
        me.form.reset()
        me._search()
    },
    _search         : function () {
        var me = this
        var values = me.form.getValues();
        me.store.load({params: values})
        me.viewModel.set({sys_constant_app_id: null})
    },
    _groupReset     : function () {
        var me = this
        me.groupform.reset()
        me._groupSearch()
    },
    _groupSearch    : function () {
        var me = this
        var values = me.groupform.getValues();
        me.groupStore.load({params: values})
    },
    _griditemclick  : function (view, record, item, index, e, eOpts) {
        var me = this;
        me.groupStore.load({params: {sys_constant_app_id: record.get('sys_constant_app_id')}})
        me.appRecord = record;
        me.viewModel.set({sys_constant_app_id: record.get('sys_constant_app_id')})
    },
    _addApp         : function () {
        var me = this;
        me._addEditWin();
    },
    _editApp        : function (grid, rowIdx, colIdx, btn, e, record, dom) {
        var me = this;
        me._addEditWin(record);
    },
    _addEditWin     : function (record) {
        var me = this;
        Ext.create('app.pages.app.AddEditWin', {appStore: me.store, appRec: record}).show();
    },
    _delApp         : function (grid, rowIdx, colIdx, btn, e, record, dom) {
        var me = this;
        app.util.Msg.confirm('提醒', Ext.String.format('确认要删除APP【{0}】吗?', record.get('app_name'))).then(function () {
            app.svc.AppSvc.appDel.call(me, record.get('sys_constant_app_id')).then(function () {
                me.store.remove(record);
            })
        })
    },
    _delAppGroup : function (grid, rowIdx, colIdx, btn, e, record, dom) {
        var me = this;
        app.util.Msg.confirm('提醒', Ext.String.format('确认要删除APP组【{0}】吗?', record.get('app_group_name'))).then(function () {
            app.svc.AppGroupSvc.del.call(me,record.get('sys_constant_app_group_id')).then(function () {
                me.groupStore.remove(record);
            })
        })
    },
    _createCode     : function (grid, rowIdx, colIdx, btn, e, record, dom) {
        var me = this;
        app.util.Msg.confirm('提醒', Ext.String.format('确认要生成应用【{0}】的代码吗?', record.get('app_key'))).then(function () {
            app.svc.AppSvc.appExec.call(me, record.get('app_key')).then(function (data) {
                app.util.Msg.toast('生成成功')
            })
        })
    },
    _addAppGroup    : function () {
        var me = this;
        me._groupAddEditWin();
    },
    _editAppGroup   : function (grid, rowIdx, colIdx, btn, e, record, dom) {
        var me = this;
        me._groupAddEditWin(record);
    },
    _groupAddEditWin: function (record) {
        var me = this;
        Ext.create('app.pages.app.GroupAddEditWin', {appGroupStore: me.groupStore, appGroupRec: record}).show();
    },
});
