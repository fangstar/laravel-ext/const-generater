/**
 * Copyright (c) 2015-2016, fangstar.com
 *
 * All rights reserved.
 */

/**
 * 数据表store的抽象类
 * 如果需要使用model的话请使用columns属性，该属性即为model中的fields属性
 * @param {type} param1
 * @param {type} param2
 */
Ext.define('app.data.store.AbstractStore', {
    extend        : 'Ext.data.Store',
    remoteSort    : true,
    autoLoad      : true,
    pageSize      : 200,
    fields        : ['id'],
    url           : null, //
    base64Decode  : false,
    proxyParams   : {},
    requires      : ['app.util.Msg'],
    model         : Ext.create('Ext.data.Model'),
    proxy         : {
        headers   : {'X-CSRF-TOKEN': Ext.util.Cookies.get('XSRF-TOKEN')},
        type      : 'ajax',
        url       : '',
        limitParam: 'pagesize',
        reader    : {
            readRecordsOnFailure: false,
            type                : 'json',
            rootProperty        : 'data',
            messageProperty     : 'msg'
        }
    },
    constructor   : function (config) {
        var self = this;
        self.callParent(arguments);
        var proxy = self.getProxy();
        for (var index in self.proxyParams) {
            if (Ext.Array.contains([] || index)) {
                continue;
            }
            proxy[index] = self.proxyParams[index];
        }
        proxy.url = self.url;
    },
    reload        : function () {
        var me = this;
        if (!me.isReqComplete()) {
            return;
        }
        return me.callParent(arguments);
    },
    load          : function (options) {
        var me       = this,
            pageSize = me.getPageSize(),
            session;
        if (!me.isReqComplete()) {
            return;
        }
        if (typeof options === 'function') {
            options = {
                callback: options
            };
        } else {
            options = Ext.apply({}, options);
        }
        //
        if (options && options.params) {
            me.lastParams = options.params;
        } else {
            options.params = me.lastParams || {};
        }
        //
        if (me._extraParams) {
            options.params = Ext.apply(me._extraParams, options.params);
        }
        // Only add grouping options if grouping is remote
        if (me.getRemoteSort() && !options.grouper && me.getGrouper()) {
            options.grouper = me.getGrouper();
        }

        if (pageSize || 'start' in options || 'limit' in options || 'page' in options) {
            options.page = options.page != null ? options.page : me.currentPage;
            options.start = (options.start !== undefined) ? options.start : (options.page - 1) * pageSize;
            options.limit = options.limit != null ? options.limit : pageSize;
        }

        options.addRecords = options.addRecords || false;

        if (!options.recordCreator) {
            session = me.getSession();
            if (session) {
                options.recordCreator = session.recordCreator;
            }
        }
        me._lastParams = options;
        return me.callParent([options]);
    },
    getLastPatams : function () {
        var self = this;
        var param = Ext.apply({}, self._lastParams);
        Ext.apply(param, param.params);
        delete param.params;
        return param;
    },
    setExtraParams: function (params) {
        var self = this;
        self._extraParams = params;
    },
    getExtraParams: function () {
        var self = this;
        return self._extraParams;
    },
    update        : function (record, data) {
        record.set(data);
        Ext.apply(record.raw, data);
    },
    /**
     * 默认为包含被过滤的记录
     * @param {type} noFilter
     * @returns {Array|Array.getAllRecords.records}
     */
    getAllRecords: function (noFilter) {
        var self = this;
        var records = [];
        if (noFilter) {
            self.each(function (record) {
                records.push(record);
            });
        } else {
            self.queryBy(function (record, id) {
                records.push(record);
                return true;
            });
        }
        return records;
    },
    /**
     *
     * @param {type} excludeFilter 是否排除被过滤的数据
     * @returns {undefined}
     */
    getAllRawData: function (excludeFilter) {
        var self = this;
        var iterp = [];
        if (excludeFilter) {
            self.each(function (record) {
                iterp.push(record.getData());
            });
        } else {
            self.queryBy(function (record, id) {
                iterp.push(record.getData());
                return true;
            });
        }
        return iterp;
    },
    abort        : function () {
        var self = this;
        self.getProxy().abort();
    },
    isReqComplete: function () {
        var self = this;
        var proxy = self.getProxy();
        var req = proxy.lastRequest;
        if (req) {
            return false;
        }
        return true;
    },
    getRawData   : function (name, value) {
        var self = this;
        var data = self.queryBy(function (record) {
            if (record.get(name) == value) {
                return true;
            }
        });
        return data.items;
    },
    getFirstData : function (name, value) {
        var self = this;
        var data = self.queryBy(function (record) {
            if (record.get(name) == value) {
                return true;
            }
        });
        if (data.items && data.items.length > 0) {
            return data.items[0].getData()
        }
        return null
    },
    listeners    : {
        load: function (store, records, successful, eOpts) {
            var self = this;
            var resp = eOpts.getResponse();
            var obj = {};
            if (eOpts.exception) {
                if (eOpts.error.status == 512) {
                    app.util.Msg.error('对不起，服务繁忙，请稍候再试');
                } else if (eOpts.error.response) {
                    if (eOpts.error.response.timedout) {
                        app.util.Msg.error('服务请求超时，请稍后重试！');
                    } else if (!eOpts.error.response.aborted) {
                        obj = Ext.JSON.decode(eOpts.error.response.responseText);
                    }
                } else if (!resp || resp.status != 200) {
                    app.util.Msg.error(eOpts.error);
                    return;
                } else {
                    var responseText = resp.responseText;
                    obj = Ext.decode(responseText, true);
                    if (Ext.isEmpty(obj)) {
                        app.util.Msg.error('后台返回的数据不符合规范<br/>' + responseText);
                        return;
                    }
                }
            } else {
                var responseText = resp.responseText;
                obj = Ext.decode(responseText, true);
            }
            if (!obj.success) {
                app.util.Msg.error(obj.msg);
                return;
            }
            self.fireEvent('afterLoad', store, records, successful, eOpts);
        }
    }
});