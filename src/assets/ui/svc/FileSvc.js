Ext.define('app.svc.FileSvc', {
    requires: ['app.svc.BaseSvc'],
    statics : {
        fileStore : function () {
            return Ext.create('app.data.store.AbstractStore', {
                url: '/const-generater/file/query-list'
            });
        },
        fileList  : function () {
            return app.svc.BaseSvc.ajax.call(this, {url: '/const-generater/file/query-list'});
        },
        fileDetail: function (path, type) {
            return app.svc.BaseSvc.ajax.call(this, {url: '/const-generater/file/detail', jsonData: Ext.JSON.encode({path: path, type: type})});
        },
    }
})