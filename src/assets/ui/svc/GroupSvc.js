Ext.define('app.svc.GroupSvc', {
    requires: ['app.svc.BaseSvc'],
    statics : {
        groupStore    : function (autoLoad) {
            return Ext.create('app.data.store.AbstractStore', {
                autoLoad: autoLoad,
                url     : '/const-generater/group/query-list'
            });
        },
        groupAdd      : function (params) {
            return app.svc.BaseSvc.ajax.call(this, {url: '/const-generater/group/add', jsonData: Ext.JSON.encode(params)});
        },
        groupUpd      : function (params) {
            return app.svc.BaseSvc.ajax.call(this, {url: '/const-generater/group/upd', jsonData: Ext.JSON.encode(params)});
        },
        groupDel      : function (groupId) {
            return app.svc.BaseSvc.ajax.call(this, {url: '/const-generater/group/del', jsonData: Ext.JSON.encode({sys_constant_group_id: groupId})});
        },
        groupQueryRel   : function (constGroupIds) {
            return app.svc.BaseSvc.ajax.call(this, {url: '/const-generater/group/query-rel-group', jsonData: Ext.JSON.encode({sys_constant_group_ids: constGroupIds})});
        },
        groupAddRel   : function (constGroupIds, appGroupId) {
            return app.svc.BaseSvc.ajax.call(this, {url: '/const-generater/group/add-rel', jsonData: Ext.JSON.encode({sys_constant_group_ids: constGroupIds, sys_constant_app_group_id: appGroupId})});
        },
        groupRemoveRel: function (constGroupIds, appGroupIds) {
            return app.svc.BaseSvc.ajax.call(this, {url: '/const-generater/group/remove-rel', jsonData: Ext.JSON.encode({sys_constant_group_ids: constGroupIds, sys_constant_app_group_ids: appGroupIds})});
        }
    }
})
