Ext.define('app.svc.ConstantSvc', {
    requires: ['app.svc.BaseSvc'],
    statics : {
        constantStore: function (autoLoad) {
            return Ext.create('app.data.store.AbstractStore', {
                autoLoad: autoLoad,
                url     : '/const-generater/constant/query-list'
            });
        },
        constantAdd  : function (params) {
            return app.svc.BaseSvc.ajax.call(this, {url: '/const-generater/constant/add', jsonData: Ext.JSON.encode(params)});
        },
        constantUpd  : function (params) {
            return app.svc.BaseSvc.ajax.call(this, {url: '/const-generater/constant/upd', jsonData: Ext.JSON.encode(params)});
        },
        constantDel  : function (groupId) {
            return app.svc.BaseSvc.ajax.call(this, {url: '/const-generater/constant/del', jsonData: Ext.JSON.encode({sys_constant_group_id: groupId})});
        }
    }
})