Ext.define('app.svc.AppGroupRelSvc', {
    requires: ['app.svc.BaseSvc'],
    statics : {
        relStoreByAppId  : function () {
            return Ext.create('app.data.store.AbstractStore', {
                autoLoad: false,
                url     : '/const-generater/app-group-rel/query-by-app-id'
            });
        },
        relStoreByGroupId: function () {
            return Ext.create('app.data.store.AbstractStore', {
                autoLoad: false,
                url     : '/const-generater/app-group-rel/query-by-group-id'
            });
        },
        relStoreBackPath: function () {
            return Ext.create('app.data.store.AbstractStore', {
                url     : '/const-generater/app-group-rel/path-list?type=back'
            });
        },
        relStoreFrontPath: function () {
            return Ext.create('app.data.store.AbstractStore', {
                url     : '/const-generater/app-group-rel/path-list?type=front'
            });
        },
        relAdd           : function (params) {
            return app.svc.BaseSvc.ajax.call(this, {url: '/const-generater/app-group-rel/add-rel', jsonData: Ext.JSON.encode(params)});
        },
        relUpd           : function (params) {
            return app.svc.BaseSvc.ajax.call(this, {url: '/const-generater/app-group-rel/upd-rel', jsonData: Ext.JSON.encode(params)});
        },
        relDel           : function (relId) {
            return app.svc.BaseSvc.ajax.call(this, {url: '/const-generater/app-group-rel/del-rel', jsonData: Ext.JSON.encode({sys_constant_app_group_rel_id: relId})});
        }
    }
})
