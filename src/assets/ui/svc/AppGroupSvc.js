Ext.define('app.svc.AppGroupSvc', {
    requires: ['app.svc.BaseSvc'],
    statics : {
        nameStore: function (autoload) {
            return Ext.create('app.data.store.AbstractStore', {
                autoLoad: autoload,
                url     : '/const-generater/app-group/query-name'
            });
        },
        store    : function (autoload) {
            return Ext.create('app.data.store.AbstractStore', {
                autoLoad: autoload,
                url     : '/const-generater/app-group/query-list'
            });
        },
        add      : function (params) {
            return app.svc.BaseSvc.ajax.call(this, {url: '/const-generater/app-group/add', jsonData: Ext.JSON.encode(params)});
        },
        upd      : function (params) {
            return app.svc.BaseSvc.ajax.call(this, {url: '/const-generater/app-group/upd', jsonData: Ext.JSON.encode(params)});
        },
        del      : function (appId) {
            return app.svc.BaseSvc.ajax.call(this, {url: '/const-generater/app-group/del', jsonData: Ext.JSON.encode({sys_constant_app_group_id: appId})});
        }
    }
})
