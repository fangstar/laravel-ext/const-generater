Ext.define('app.svc.AppSvc', {
    requires: ['app.svc.BaseSvc'],
    statics : {
        appNameStore: function (autoload) {
            return Ext.create('app.data.store.AbstractStore', {
                autoLoad: autoload,
                url     : '/const-generater/app/query-name'
            });
        },
        appStore    : function () {
            return Ext.create('app.data.store.AbstractStore', {
                url: '/const-generater/app/query-list'
            });
        },
        appAdd      : function (params) {
            return app.svc.BaseSvc.ajax.call(this, {url: '/const-generater/app/add', jsonData: Ext.JSON.encode(params)});
        },
        appUpd      : function (params) {
            return app.svc.BaseSvc.ajax.call(this, {url: '/const-generater/app/upd', jsonData: Ext.JSON.encode(params)});
        },
        appDel      : function (appId) {
            return app.svc.BaseSvc.ajax.call(this, {url: '/const-generater/app/del', jsonData: Ext.JSON.encode({sys_constant_app_id: appId})});
        },
        appExec     : function (appKey) {
            return app.svc.BaseSvc.ajax.call(this, {url: '/const-generater/app/exec-const-generater', jsonData: Ext.JSON.encode({app_key: appKey})});
        }
    }
})
