Ext.define('app.svc.BaseSvc', function () {
    function mask(conf) {
        let me = this
        if (me instanceof Ext.Component) {
            conf.mask = me;
        } else {
            var comps = Ext.ComponentQuery.query('viewport');
            if (comps.length > 0) {
                conf.mask = comps[0]
            }
        }
        if (conf.mask && conf.mask.isVisible && conf.mask.isVisible()) {
            conf.mask.mask(conf.loadMsg || '载入中...')
        }
    }

    function unmask(conf) {
        if (conf.mask && conf.mask.isMasked()) {
            conf.mask.unmask()
        }
    }

    function parseResp(resp, resolve, reject, conf) {
        var obj = Ext.decode(resp.responseText, true);
        if (obj.success) {
            resolve(obj.data)
        } else {
            reject({msg: obj.msg, code: obj.errorcode});
            if (!conf.hideErrMsg) {
                Ext.Msg.show({title: '错误', msg: obj.msg, buttonText: {yes: '确定'}, buttons: Ext.Msg.YES, icon: Ext.Msg.ERROR});
            }
        }
        unmask.call(this, conf)
    }

    return {
        requires: ['app.data.store.AbstractStore'],
        statics : {
            ajax     : function (conf) {
                var me = this
                if (!conf.hideMask) {
                    mask.call(me, conf)
                }
                return new Ext.Promise(function (resolve, reject) {
                    var params = Ext.apply({
                                               success: function (resp, opts) {
                                                   parseResp.call(me, resp, resolve, reject, conf)
                                               },
                                               failure: function (resp, opts) {
                                                   var msg = 'HTTP [' + resp.status + '] ' + resp.statusText
                                                   var data = Ext.decode(resp.responseText, true)
                                                   if (data && data.msg) {
                                                       msg = data.msg
                                                   }
                                                   if (!conf.hideErrMsg) {
                                                       Ext.Msg.show({title: '错误', msg: msg, buttonText: {yes: '确定'}, buttons: Ext.Msg.YES, icon: Ext.Msg.ERROR});
                                                   }
                                                   reject({msg: msg, status: resp.status, code: (data && data.errorcode) || 0});
                                                   unmask.call(this, conf)
                                               }
                                           }, conf)
                    Ext.Ajax.request(params);
                })

            },
            parseResp: parseResp
        }
    }
})
