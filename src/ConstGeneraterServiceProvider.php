<?php

namespace Fstar\ConstGenerater;

use Fstar\ConstGenerater\Impl\ConstGeneraterParse;
use Illuminate\Support\ServiceProvider;

class ConstGeneraterServiceProvider extends ServiceProvider {
    public function boot() {
        $this->publishes([__DIR__ . "/config/fstar-const-generater.php" => config_path(Constants::conf_name . ".php")]);
        $this->publishes([__DIR__ . "/assets" => public_path("js/const-generater")], 'const-generater-public');
        if($this->app->runningInConsole()) {
            $this->commands([ConstGeneraterCommand::class]);
        }
        $this->loadRoutesFrom(__DIR__ . '/routes/web.php');
        $this->loadMigrationsFrom(__DIR__ . '/database/migrations');
        $this->loadViewsFrom(__DIR__ . '/resources/views', 'const-generater');
    }
}