<?php

use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'const-generater', 'namespace' => 'Fstar\ConstGenerater\Http', 'middleware' => [Fstar\ConstGenerater\Http\Middleware\EnvCheck::class]], function() {
    Route::get('/', function() {
        return view('const-generater::index');
    });
    Route::get('app/query-name', 'ConstAppController@queryAppName');
    Route::get('app/query-list', 'ConstAppController@queryList');
    Route::any('app/add', 'ConstAppController@add');
    Route::any('app/upd', 'ConstAppController@upd');
    Route::any('app/del', 'ConstAppController@del');
    Route::any('app/exec-const-generater', 'ConstAppController@execConstGenerater');

    Route::get('group/query-list', 'ConstGroupController@queryList');
    Route::any('group/add', 'ConstGroupController@add');
    Route::any('group/upd', 'ConstGroupController@upd');
    Route::any('group/del', 'ConstGroupController@del');
    Route::any('group/query-rel-group', 'ConstGroupController@queryRelGroup');
    Route::any('group/add-rel', 'ConstGroupController@addRel');
    Route::any('group/remove-rel', 'ConstGroupController@removeRel');

    Route::get('constant/query-list', 'ConstantController@queryList');
    Route::any('constant/add', 'ConstantController@add');
    Route::any('constant/upd', 'ConstantController@upd');
    Route::any('constant/del', 'ConstantController@del');

    Route::get('app-group/query-name', 'ConstAppGroupController@queryAppGroupName');
    Route::get('app-group/query-list', 'ConstAppGroupController@queryList');
    Route::any('app-group/add', 'ConstAppGroupController@add');
    Route::any('app-group/upd', 'ConstAppGroupController@upd');
    Route::any('app-group/del', 'ConstAppGroupController@del');

    Route::any('file/query-list', 'FileController@fileList');
    Route::any('file/detail', 'FileController@fileDetail');
});
