<?php

namespace Fstar\ConstGenerater\Services;

use Illuminate\Support\Facades\DB;

class BaseService {
    protected function getConn($table = null) {
        $db   = config('fstar-const-generater.db_conn');
        $conn = DB::connection($db);
        if(!empty($table)) {
            $conn = $conn->table($table);
        }
        return $conn;
    }
}