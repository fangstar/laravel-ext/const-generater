<?php

namespace Fstar\ConstGenerater\Services;

use Fstar\ConstGenerater\Constants;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Mockery\Exception;

class ConstGroupService extends BaseService {

    public function queryList($params, $page, $sort) {
        $builder = $this->getConn('sys_constant_group as g')
                        ->leftJoin('sys_constant_app_group_rel as rel', function($join) {
                            $join->on('rel.sys_constant_group_id', '=', 'g.sys_constant_group_id')
                                 ->where('rel.delete_flag', Constants::DEL_NO);
                        })
                        ->leftJoin('sys_constant_app_group as ag', function($join) {
                            $join->on('ag.sys_constant_app_group_id', '=', 'rel.sys_constant_app_group_id')
                                 ->where('ag.delete_flag', Constants::DEL_NO);
                        })
                        ->leftJoin('sys_constant_app as a', 'a.sys_constant_app_id', '=', 'ag.sys_constant_app_id')
                        ->where('g.delete_flag', Constants::DEL_NO);
        if(!empty($params['sys_constant_group_id'])) {
            $builder->where('g.sys_constant_group_id', $params['sys_constant_group_id']);
        }
        if(!empty($params['sys_constant_app_id'])) {
            $builder->where('a.sys_constant_app_id', $params['sys_constant_app_id']);
        }
        if(!empty($params['sys_constant_app_group_id'])) {
            $builder->where('ag.sys_constant_app_group_id', $params['sys_constant_app_group_id']);
        }
        if(!empty($params['group_name'])) {
            $builder->where(function(Builder $query) use ($params) {
                $query->where('g.group_key', 'like', "%{$params['group_name']}%")
                      ->orWhere('g.group_name', 'like', "%{$params['group_name']}%");
            });
        }
        $builder->where('g.delete_flag', Constants::DEL_NO);
        $cnt  = $builder->clone()->distinct('g.sys_constant_group_id')->count();
        $data = $builder->groupBy('g.sys_constant_group_id')
                        ->orderBy($sort['field'], $sort['direction'])
                        ->skip($page['skip'])
                        ->take($page['pagesize'])
                        ->get(['g.*', 'rel.sys_constant_app_group_id', 'ag.sys_constant_app_id', DB::raw("group_concat(a.app_name) as app_name,
                        group_concat(ag.app_group_name) as app_group_name,
                        group_concat(concat('【',a.app_name,'->',ag.app_group_name,'】 【',ag.group_back_path,'】 【',ag.group_front_path,'】')) as group_info")]);
        return ['data' => $data, 'total' => $cnt];
    }

    public function findById($group_id) {
        $data = $this->queryList(['sys_constant_group_id' => $group_id], ['skip' => 0, 'pagesize' => 1], ['field' => 'g.sys_constant_group_id', 'direction' => 'asc']);
        return data_get($data, 'data.0', []);
    }

    public function add($params) {
        $group                = Arr::only($params, ['group_key', 'group_name']);
        $group['created_at']  = time();
        $group['delete_flag'] = Constants::DEL_NO;
        $rel                  = Arr::only($params, ['sys_constant_app_group_id']);
        $rel['created_at']    = time();
        $rel['delete_flag']   = Constants::DEL_NO;
        $conn                 = $this->getConn();
        $conn->beginTransaction();
        try {
            $group_id                     = $conn->table('sys_constant_group')->insertGetId($group);
            $rel['sys_constant_group_id'] = $group_id;
            $conn->table('sys_constant_app_group_rel')->insert($rel);
            $conn->commit();
        } catch(Exception $ex) {
            $conn->rollBack();
            throw $ex;
        }
        return $this->findById($group_id);
    }

    public function upd($params) {
        $params['updated_at'] = time();
        $builder              = $this->getConn('sys_constant_group');
        $builder->where('sys_constant_group_id', $params['sys_constant_group_id'])->update($params);
        return $builder->where('sys_constant_group_id', $params['sys_constant_group_id'])->first();
    }

    public function del($group_id) {
        $conn    = $this->getConn();
        $upd_cnt = $conn->table('sys_constant_group')
                        ->where('sys_constant_group_id', $group_id)
                        ->update(['delete_flag' => Constants::DEL_YES]);
        $conn->table('sys_constant_app_group_rel')
             ->where('sys_constant_group_id', $group_id)
             ->update(['delete_flag' => Constants::DEL_YES, 'del_rel_id' => DB::raw('sys_constant_group_id')]);
        return $upd_cnt;
    }

    public function queryRelGroup(array $const_group_ids) {
        return $this->getConn('sys_constant_group as g')
                    ->join('sys_constant_app_group_rel as rel', 'rel.sys_constant_group_id', '=', 'g.sys_constant_group_id')
                    ->join('sys_constant_app_group as ag', 'ag.sys_constant_app_group_id', '=', 'rel.sys_constant_app_group_id')
                    ->join('sys_constant_app as a', 'a.sys_constant_app_id', '=', 'ag.sys_constant_app_id')
                    ->whereIn('g.sys_constant_group_id', $const_group_ids)
                    ->where('g.delete_flag', Constants::DEL_NO)
                    ->where('rel.delete_flag', Constants::DEL_NO)
                    ->where('ag.delete_flag', Constants::DEL_NO)
                    ->groupBy('ag.sys_constant_app_group_id')
                    ->get(['a.app_name', 'ag.sys_constant_app_group_id', 'ag.app_group_name']);
    }

    public function addRel(array $const_group_ids, $app_group_id) {
        $conn            = $this->getConn();
        $exist_group_ids = $conn->table('sys_constant_app_group_rel')
                                ->where('delete_flag', Constants::DEL_NO)
                                ->whereIn('sys_constant_group_id', $const_group_ids)
                                ->where('sys_constant_app_group_id', $app_group_id)
                                ->get(['sys_constant_group_id'])->pluck('sys_constant_group_id')->toArray();
        $diff_group_ids  = array_diff($const_group_ids, $exist_group_ids);
        if(count($diff_group_ids) > 0) {
            $ins_arry = [];
            $time     = time();
            foreach($diff_group_ids as $group_id) {
                $ins_arry[] = ['sys_constant_app_group_id' => $app_group_id, 'sys_constant_group_id' => $group_id, 'created_at' => $time, 'delete_flag' => Constants::DEL_NO];
            }
            $conn->table('sys_constant_app_group_rel')->insert($ins_arry);
        }
        return $diff_group_ids;
    }

    public function removeRel(array $const_group_ids, array $app_group_ids) {
        return $this->getConn('sys_constant_app_group_rel')->whereIn('sys_constant_group_id', $const_group_ids)
                    ->whereIn('sys_constant_app_group_id', $app_group_ids)
                    ->where('delete_flag', Constants::DEL_NO)
                    ->update(['delete_flag' => Constants::DEL_YES, 'del_rel_id' => DB::raw('sys_constant_group_id')]);
    }
}
