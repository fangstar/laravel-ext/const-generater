<?php

namespace Fstar\ConstGenerater\Services;

use Fstar\ConstGenerater\Constants;
use Illuminate\Support\Facades\File;

class FileService extends BaseService {
    const TYPE_BACK  = 'back';
    const TYPE_FRONT = 'front';

    public function fileList() {
        $app_key    = config('fstar-const-generater.app_key');
        $builder    = $this->getConn('sys_constant_app');
        $appM       = $builder->where('delete_flag', Constants::DEL_NO)
                              ->where('app_key', $app_key)
                              ->first(['app_back_path', 'app_front_path', 'app_name']);
        $back_list  = $this->queryList(base_path(), $appM->app_back_path, self::TYPE_BACK);
        $front_list = $this->queryList(base_path(), $appM->app_front_path, self::TYPE_FRONT);
        return ['back_info' => $back_list, 'front_info' => $front_list];
    }

    public function fileContent($path, $type) {
        $app_key = config('fstar-const-generater.app_key');
        $field   = $type == self::TYPE_BACK ? 'app_back_path as app_path' : 'app_front_path as app_path';
        $builder = $this->getConn('sys_constant_app');
        $appM    = $builder->where('delete_flag', Constants::DEL_NO)
                           ->where('app_key', $app_key)
                           ->first([$field]);
        $sep     = DIRECTORY_SEPARATOR;
        $path    = base_path("{$appM->app_path}{$sep}{$path}");
        return file_get_contents($path);
    }

    private function queryList($base_dir, $dir, $type = self::TYPE_BACK) {
        $idx     = 1;
        $list    = [];
        $nodes   = [];
        $sep     = DIRECTORY_SEPARATOR;
        $dir     = $this->trimDir($dir);
        $path    = "{$base_dir}{$sep}{$dir}";
        $dir_map = [$dir => ['id' => $dir, 'pid' => $dir, 'text' => 'root', 'children' => []]];
        $dirs    = [$path];
        $data    = [];
        do {
            $tmp_dir  = array_pop($dirs);
            $tmp_dirs = File::directories($tmp_dir);
            foreach($tmp_dirs as $_dir) {
                $id           = $this->trimDir(str_replace($base_dir, '', $_dir));
                $tmp_path     = str_replace('\\', '/', $_dir);
                $dir_map[$id] = [
                    'id'       => $id,
                    'pid'      => $this->trimDir(str_replace($base_dir, '', $tmp_dir)),
                    'text'     => substr($tmp_path, strrpos($tmp_path, '/') + 1),
                    'leaf'     => false,
                    'children' => []
                ];
            }
            $dirs = array_merge($dirs, $tmp_dirs);
        } while(count($dirs) > 0);

        foreach($dir_map as $c_dir => $item) {
            $files = File::files("{$base_dir}{$sep}{$c_dir}");
            $list  = [];
            foreach($files as $file) {
                $file_name = $file->getFilename();
                $list[]    = [
                    'text' => $file_name,
                    'path' => $this->trimDir(str_replace($dir, '', "{$c_dir}{$sep}{$file_name}")),
                    'leaf' => true,
                    'type' => $type,
                    'size' => $file->getSize()
                ];
            }
            $dir_map[$c_dir]['children'] = $list;
        }
        return $dir_map;
    }

    private function trimDir($path) {
        $paths = explode('/', str_replace('\\', '/', $path));
        $dirs  = [];
        foreach($paths as $path) {
            if(!empty($path)) {
                $dirs[] = $path;
            }
        }
        return implode(DIRECTORY_SEPARATOR, $dirs);
    }
}