<?php

namespace Fstar\ConstGenerater\Services;

use Fstar\ConstGenerater\Constants;
use Illuminate\Database\Query\Builder;

class ConstAppGroupService extends BaseService {
    
    public function queryAppGroupName($params) {
        $builder = $this->getConn('sys_constant_app_group')
                        ->where('delete_flag', Constants::DEL_NO);
        if(!empty($params['sys_constant_app_id'])) {
            $builder->where('sys_constant_app_id', $params['sys_constant_app_id']);
        }
        if(!empty($params['keyword'])) {
            $builder->where('app_group_name', 'like', "%{$params['keyword']}%");
        }
        return $builder->get(['sys_constant_app_group_id as value', 'app_group_name as text']);
    }

    public function queryList($params, $page, $sort) {
        $builder = $this->getConn('sys_constant_app_group as g');
        $builder->join('sys_constant_app as a', 'a.sys_constant_app_id', '=', 'g.sys_constant_app_id')
                ->where('g.delete_flag', Constants::DEL_NO);
        if(!empty($params['sys_constant_app_id'])) {
            $builder->where('a.sys_constant_app_id', $params['sys_constant_app_id']);
        }
        if(!empty($params['keyword'])) {
            $keyword = $params['keyword'];
            $builder->where(function(Builder $query) use ($keyword) {
                $query->Where('g.app_group_name', 'like', "%{$keyword}%")
                      ->orWhere('g.group_back_path', 'like', "%{$keyword}%")
                      ->orWhere('g.group_front_path', 'like', "%{$keyword}%");
            });
        }
        $cnt  = $builder->count('g.sys_constant_app_group_id');
        $data = $builder->orderBy($sort['field'], $sort['direction'])
                        ->skip($page['skip'])
                        ->take($page['pagesize'])
                        ->get(['a.app_name', 'g.*']);
        return ['data' => $data, 'total' => $cnt];
    }

    public function queryById($app_group_id) {
        return $this->getConn('sys_constant_app_group as g')
                    ->join('sys_constant_app as a', 'a.sys_constant_app_id', '=', 'g.sys_constant_app_id')
                    ->where('g.delete_flag', Constants::DEL_NO)
                    ->where('g.sys_constant_app_group_id', $app_group_id)
                    ->first(['g.*', 'a.app_name']);
    }

    public function add($params) {
        $params  = array_merge($params, [
            'created_at' => time()
        ]);
        $builder = $this->getConn('sys_constant_app_group');
        $rel_id  = $builder->insertGetId($params);
        return $this->queryById($rel_id);
    }

    public function upd($params) {
        $builder = $this->getConn('sys_constant_app_group');
        $builder->where('sys_constant_app_group_id', $params['sys_constant_app_group_id'])->update($params);
        return $this->queryById($params['sys_constant_app_group_id']);
    }

    public function del($rel_id) {
        $builder = $this->getConn('sys_constant_app_group');
        return $builder->where('sys_constant_app_group_id', $rel_id)
                       ->update([
                                    'del_rel_id'  => $rel_id,
                                    'delete_flag' => Constants::DEL_YES,
                                ]);
    }
}
