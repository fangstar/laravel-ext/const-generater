<?php

namespace Fstar\ConstGenerater\Services;

use Fstar\ConstGenerater\Constants;
use Fstar\ConstGenerater\Impl\ConstGeneraterParse;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\DB;

class ConstAppService extends BaseService {

    public function queryAppName() {
        return $builder = $this->getConn('sys_constant_app')
                               ->where('delete_flag', Constants::DEL_NO)
                               ->get(['sys_constant_app_id as value', 'app_name as text']);
    }

    public function queryList($params) {
        $builder = $this->getConn('sys_constant_app');
        if(!empty($params['app_name'])) {
            $builder->where(function(Builder $query) use ($params) {
                $query->where('app_key', 'like', "%{$params['app_name']}%")
                      ->orWhere('app_name', 'like', "%{$params['app_name']}%");
            });
        }
        $data = $builder->where('delete_flag', Constants::DEL_NO)->get()->toArray();
        return ['data' => $data, 'total' => count($data)];
    }

    public function add($params) {
        $params['created_at']  = time();
        $params['delete_flag'] = Constants::DEL_NO;
        $builder               = $this->getConn('sys_constant_app');
        $app_id                = $builder->insertGetId($params);
        return $builder->where('sys_constant_app_id', $app_id)->first();
    }

    public function upd($params) {
        $params['updated_at'] = time();
        $builder              = $this->getConn('sys_constant_app');
        $builder->where('sys_constant_app_id', $params['sys_constant_app_id'])->update($params);
        return $builder->where('sys_constant_app_id', $params['sys_constant_app_id'])->first();
    }

    public function del($app_id) {
        $builder = $this->getConn('sys_constant_app');
        return $builder->where('sys_constant_app_id', $app_id)->update(['delete_flag' => Constants::DEL_YES]);
    }

    public function execConstGenerater($app_key, $log = null) {
        $conn              = DB::connection(config('fstar-const-generater.db_conn'));
        $bg_file_map       = [];
        $front_file_map    = [];
        $back_impl         = config('fstar-const-generater.back_impl');
        $front_impl        = config('fstar-const-generater.front_impl');
        $need_create_back  = !empty($back_impl);
        $need_create_front = !empty($front_impl);
        $conn->table('sys_constant as c')
             ->join('sys_constant_group as g', 'c.sys_constant_group_id', '=', 'g.sys_constant_group_id')
             ->join('sys_constant_app_group_rel as rel', 'rel.sys_constant_group_id', '=', 'g.sys_constant_group_id')
             ->join('sys_constant_app_group as ag', 'ag.sys_constant_app_group_id', '=', 'rel.sys_constant_app_group_id')
             ->join('sys_constant_app as a', 'a.sys_constant_app_id', '=', 'ag.sys_constant_app_id')
             ->where('a.app_key', $app_key)
             ->where('a.delete_flag', Constants::DEL_NO)
             ->where('c.delete_flag', Constants::DEL_NO)
             ->where('g.delete_flag', Constants::DEL_NO)
             ->where('ag.delete_flag', Constants::DEL_NO)
             ->where('rel.delete_flag', Constants::DEL_NO)
             ->orderBy('constant_sort')
             ->get(['a.app_back_path', 'a.app_back_ns', 'a.app_front_path', 'a.app_front_ns', 'g.sys_constant_group_id', 'g.group_name', 'g.group_key', 'ag.group_back_path', 'ag.group_front_path', 'c.constant_color', 'c.constant_key', 'c.constant_name', 'c.constant_val', 'c.constant_val_type'])
             ->groupBy('sys_constant_group_id')
             ->each(function($collect) use (&$bg_file_map, &$front_file_map, $need_create_back, $need_create_front) {
                 $key_len      = 0;
                 $constant_len = 0;
                 $val_len      = 0;
                 $items        = $collect->map(function($constantObj) use (&$key_len, &$constant_len, &$val_len) {
                     $key          = "{$constantObj->group_key}_{$constantObj->constant_key}";
                     $key_len      = max($key_len, strlen($key));
                     $constant_len = max($constant_len, strlen($constantObj->constant_key));
                     if(in_array($constantObj->constant_val_type, [Constants::VAL_TYPE_CONST])) {

                     }
                     return ['key' => $key, 'text_color' => $constantObj->constant_color, 'constant_key' => $constantObj->constant_key, 'type' => $constantObj->constant_val_type, 'val' => $constantObj->constant_val, 'text' => $constantObj->constant_name];
                 })->toArray();
                 $itemObj      = $collect->first();
                 $group        = ['text' => $itemObj->group_name, 'group_key' => str_pad($itemObj->group_key, $key_len), 'key_len' => $key_len, 'constant_len' => $constant_len, 'items' => $items];
                 if($need_create_front) {
                     $frontpath = "{$itemObj->app_front_path}/{$itemObj->group_front_path}";
                     if(!array_key_exists($frontpath, $front_file_map)) {
                         $front_file_map[$frontpath] = ['app_ns' => $itemObj->app_front_ns, 'app_path' => $itemObj->app_front_path, 'group_path' => $itemObj->group_front_path, 'items' => []];
                     }
                     $front_file_map[$frontpath]['items'][] = $group;
                 }
                 if($need_create_back) {
                     $bgpath = "{$itemObj->app_back_path}/{$itemObj->group_back_path}";
                     if(!array_key_exists($bgpath, $bg_file_map)) {
                         $bg_file_map[$bgpath] = ['app_ns' => $itemObj->app_back_ns, 'app_path' => $itemObj->app_back_path, 'group_path' => $itemObj->group_back_path, 'items' => []];
                     }
                     $bg_file_map[$bgpath]['items'][] = $group;
                 }
             });
        $svc = new ConstGeneraterParse();
        if($need_create_front) {
            $svc->getFrontConstGeneraterParse()->createConstFile($front_file_map);
            $log && $log->info("Front const generate successful");
        }
        if($need_create_back) {
            $svc->getBackConstGeneraterParse()->createConstFile($bg_file_map);
            $log && $log->info("Back const generate successful");
        }
    }
}
