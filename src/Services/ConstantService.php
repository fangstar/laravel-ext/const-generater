<?php

namespace Fstar\ConstGenerater\Services;

use Fstar\ConstGenerater\Constants;

class ConstantService extends BaseService {
    public function queryList($group_id, $sort) {
        $builder = $this->getConn('sys_constant');
        $list    = $builder->where('sys_constant_group_id', $group_id)->orderBy($sort['field'], $sort['direction'])->get();
        return ['data' => $list, 'total' => count($list)];
    }

    public function add($params) {
        $params['created_at']  = time();
        $params['delete_flag'] = Constants::DEL_NO;
        $builder               = $this->getConn('sys_constant');
        $group_id              = $builder->insertGetId($params);
        return $builder->where('sys_constant_id', $group_id)->first();
    }

    public function upd($params) {
        $params['updated_at'] = time();
        $builder              = $this->getConn('sys_constant');
        $builder->where('sys_constant_id', $params['sys_constant_id'])->update($params);
        return $builder->where('sys_constant_id', $params['sys_constant_id'])->first();
    }

    public function del($const_id) {
        $builder = $this->getConn('sys_constant');
        return $builder->where('sys_constant_id', $const_id)->update(['delete_flag' => Constants::DEL_YES]);
    }
}