<?php

namespace Fstar\ConstGenerater\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class EnvCheck {
    public function handle(Request $request, Closure $next) {
        if(config('app.debug')) {
            return $next($request);
        } else {
            throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException("Not Found");
        }
    }
}