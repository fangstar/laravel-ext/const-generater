<?php

namespace Fstar\ConstGenerater\Http;

use Illuminate\Routing\Controller;

class BaseController extends Controller {

    protected function resp($data, int $httpCode = 200, string $contentType = 'application/json') {
        $resp = response($data, $httpCode)->header('Content-Type', $contentType);
        return $resp;
    }

    protected function respSuccessMsg(string $msg) {
        $arr        = $this->initRespData();
        $arr['msg'] = $msg;
        return $this->resp($arr);
    }

    protected function respSuccessData($data = null, string $msg = "操作成功") {
        $arr         = $this->initRespData();
        $arr['msg']  = $msg;
        $arr['data'] = $data;
        return $this->resp($arr);
    }

    protected function respSuccessDataTotal(array $data_total = ['data' => [], 'total' => 0], $msg = "查询成功") {
        $arr          = $this->initRespData();
        $arr['msg']   = $msg;
        $arr['data']  = $data_total['data'];
        $arr['total'] = $data_total['total'];
        return $this->resp($arr);
    }

    protected function respSuccess(array $data = [], string $msg = "查询成功") {
        $arr         = $this->initRespData();
        $arr['msg']  = $msg;
        $arr['data'] = $data;
        return $this->resp($arr);
    }


    protected function respError(string $msg = "系统出现异常，可拨打电话：400-0707566，或让经理在微信群中@刘宇。", $error_code = 0, $data = null) {
        $arr              = $this->initRespData();
        $arr['success']   = false;
        $arr['msg']       = $msg;
        $arr['data']      = $data;
        $arr['errorcode'] = $error_code;
        return $this->resp($arr);
    }

    protected function initRespData() {
        return [
            'success'   => true,
            'errorcode' => 0,
            'msg'       => '',
            'data'      => []
        ];
    }

    protected function getPageParamsUseDefault(bool $limit_max_size = true): array {
        $max_page_size = 500;
        $page          = request('page', 1);
        $pagesize      = request('pagesize', $max_page_size);
        if($limit_max_size) {
            if($pagesize > $max_page_size) {
                $pagesize = $max_page_size;
            }
        }
        $skip = $pagesize * ($page - 1);
        return ['page' => $page, 'skip' => $skip, 'pagesize' => $pagesize, 'limit' => $pagesize];
    }

    protected function getSortParams(string $field, string $direction = 'desc', array $sort_conf = []): array {
        $sort = request('sort', null);
        if(!is_null($sort)) {
            try {
                $sortArry = json_decode($sort, true);
                if(is_array($sortArry) && count($sortArry) > 0) {
                    if(!empty($sortArry['property'])) {
                        $field = $sortArry['property'];
                        if(!empty($sortArry['direction'])) {
                            $direction = $sortArry['direction'];
                        }
                    } else {
                        $field = $sortArry[0]['property'];
                        if(!empty($sortArry[0]['direction'])) {
                            $direction = $sortArry[0]['direction'];
                        }
                    }
                }
            } catch(\Exception $ex) {
                report($ex);
            }
        }
        return ['field' => data_get($sort_conf, $field, $field), 'direction' => $direction];
    }
}