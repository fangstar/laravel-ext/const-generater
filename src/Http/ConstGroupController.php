<?php

namespace Fstar\ConstGenerater\Http;

use Fstar\ConstGenerater\Services\ConstGroupService;
use Mockery\Exception;

class ConstGroupController extends BaseController {

    public function queryList(ConstGroupService $svc) {
        $params = request()->all();
        $page   = $this->getPageParamsUseDefault();
        $sort   = $this->getSortParams('group_name');
        $data   = $svc->queryList($params, $page, $sort);
        return $this->respSuccessDataTotal($data);
    }


    public function add(ConstGroupService $svc) {
        $params    = request()->all();
        $validator = validator($params, [
            'group_key'                 => 'required',
            'group_name'                => 'required',
            'sys_constant_app_group_id' => 'required',
        ]);
        if($validator->fails()) {
            throw new Exception($validator->errors()->first());
        }
        $data = $svc->add($params);
        return $this->respSuccessData($data);
    }


    public function upd(ConstGroupService $svc) {
        $params = request()->all();
        $data   = $svc->upd($params);
        return $this->respSuccessData($data);
    }


    public function del(ConstGroupService $svc) {
        $data = $svc->del(request('sys_constant_group_id'));
        return $this->respSuccessData($data);
    }

    public function queryRelGroup(ConstGroupService $svc) {
        $params    = request()->json()->all();
        $validator = validator($params, [
            'sys_constant_group_ids' => 'required'
        ]);
        if($validator->fails()) {
            throw new Exception($validator->errors()->first());
        }
        $data = $svc->queryRelGroup($params['sys_constant_group_ids']);
        return $this->respSuccessData($data);
    }

    public function addRel(ConstGroupService $svc) {
        $params    = request()->json()->all();
        $validator = validator($params, [
            'sys_constant_group_ids'    => 'required',
            'sys_constant_app_group_id' => 'required',
        ]);
        if($validator->fails()) {
            throw new Exception($validator->errors()->first());
        }
        $data = $svc->addRel($params['sys_constant_group_ids'], $params['sys_constant_app_group_id']);
        return $this->respSuccessData($data);
    }

    public function removeRel(ConstGroupService $svc) {
        $params    = request()->json()->all();
        $validator = validator($params, [
            'sys_constant_group_ids'     => 'required',
            'sys_constant_app_group_ids' => 'required',
        ]);
        if($validator->fails()) {
            throw new Exception($validator->errors()->first());
        }
        $data = $svc->removeRel($params['sys_constant_group_ids'], $params['sys_constant_app_group_ids']);
        return $this->respSuccessData($data);
    }
}
