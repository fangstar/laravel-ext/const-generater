<?php

namespace Fstar\ConstGenerater\Http;

use Fstar\ConstGenerater\Services\ConstAppService;

class ConstAppController extends BaseController {

    public function queryAppName(ConstAppService $svc) {
        $data = $svc->queryAppName();
        return $this->respSuccessData($data);
    }

    public function queryList(ConstAppService $svc) {
        $params = request()->all();
        $data   = $svc->queryList($params);
        return $this->respSuccessDataTotal($data);
    }

    public function add(ConstAppService $svc) {
        $params = request()->all();
        $data   = $svc->add($params);
        return $this->respSuccessData($data);
    }


    public function upd(ConstAppService $svc) {
        $params = request()->all();
        $data   = $svc->upd($params);
        return $this->respSuccessData($data);
    }


    public function del(ConstAppService $svc) {
        $data = $svc->del(request('sys_constant_app_id'));
        return $this->respSuccessData($data);
    }

    public function execConstGenerater(ConstAppService $svc) {
        $data = $svc->execConstGenerater(request('app_key'));
        return $this->respSuccessData($data);
    }
}