<?php

namespace Fstar\ConstGenerater\Http;

use Fstar\ConstGenerater\Services\ConstAppGroupService;

class ConstAppGroupController extends BaseController {

    public function queryAppGroupName(ConstAppGroupService $svc) {
        $data = $svc->queryAppGroupName(request()->all());
        return $this->respSuccessData($data);
    }

    public function queryList(ConstAppGroupService $svc) {
        $page = $this->getPageParamsUseDefault();
        $sort = $this->getSortParams('g.app_group_name');
        $data = $svc->queryList(request()->all(), $page, $sort);
        return $this->respSuccessDataTotal($data);
    }

    public function add(ConstAppGroupService $svc) {
        $params = request()->all();
        $data   = $svc->add($params);
        return $this->respSuccessData($data);
    }

    public function upd(ConstAppGroupService $svc) {
        $params = request()->all();
        $data   = $svc->upd($params);
        return $this->respSuccessData($data);
    }

    public function del(ConstAppGroupService $svc) {
        $data = $svc->del(request('sys_constant_app_group_id'));
        return $this->respSuccessData($data);
    }

}
