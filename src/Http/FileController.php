<?php

namespace Fstar\ConstGenerater\Http;

use Fstar\ConstGenerater\Services\FileService;

class FileController extends BaseController {

    public function fileList(FileService $svc) {
        $data = $svc->fileList();
        return $this->respSuccessData($data);
    }

    public function fileDetail(FileService $svc) {
        $data = $svc->fileContent(request('path'), request('type'));
        return $this->respSuccessData($data);
    }
}