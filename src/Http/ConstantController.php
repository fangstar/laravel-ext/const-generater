<?php

namespace Fstar\ConstGenerater\Http;

use Fstar\ConstGenerater\Services\ConstantService;

class ConstantController extends BaseController {
    public function queryList(ConstantService $svc) {
        $sort = $this->getSortParams('constant_sort', 'asc');
        $data = $svc->queryList(request('sys_constant_group_id'), $sort);
        return $this->respSuccessDataTotal($data);
    }

    public function add(ConstantService $svc) {
        $params = request()->all();
        $data   = $svc->add($params);
        return $this->respSuccessData($data);
    }


    public function upd(ConstantService $svc) {
        $params = request()->all();
        $data   = $svc->upd($params);
        return $this->respSuccessData($data);
    }


    public function del(ConstantService $svc) {
        $data = $svc->del(request('sys_constant_id'));
        return $this->respSuccessData($data);
    }
}