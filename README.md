#const-generater

#### 介绍
房星科技前后端常量生成工具

#### 软件架构
laravel


#### 安装教程
1. composer require fstar/const-generater
2. php artisan vendor:publish --provider=Fstar\ConstGenerater\ConstGeneraterServiceProvider
3. 执行 php artisan migrate 生成数据表 自己添加要生成常量的数据
4. 修改生成配置  config/fstar-const-generater.php
5. 打开界面配置常量 $host/const-generater
6. 执行名称 php artisan const-generater